/**************************************************************
Class   : ENT_Batch_LIHTC_Late_Notify_SetFlag
Author  : Bdarji
Date    : 02/27/2020
Details : Related ticket #EHCI-373 : 1231 Round 1 Draft Required late notice to FRC (End of Feb)
          - This batch will identidy deals and set a flag(X12_31_Document_Missing__c) is true which falls under missing document criteria.
          - criteria is mentioned on the ticket.
          - for such deal's this batch will set a flag as true, then that flag is used in query mentioned in reminder record.
Test Class : TEST_TestCoverage_EPIC14 (76%)
History : v1.0 - 02/27/2020 - Created
*****************************************************************/
global class ENT_Batch_LIHTC_Late_Notify_SetFlag implements Schedulable, Database.Batchable<sObject>, Database.Stateful
{
    global string StrQuery;
    private List<Database.SaveResult> lstDbSaveResult = new List<Database.SaveResult>();

    // Hold final result string to append in message body.
    private String final_Result_String;
    private String exception_String;

    global ENT_Batch_LIHTC_Late_Notify_SetFlag()
    {
        // get 'Monica Spillane' user details. (Id = '00530000004gy3s')
        List<User> lstOfUser = [Select Id, Name From User Where Name =: Label.ENT_LIHTC_User_Monica_Spillane limit 1];
        Id idOfMonica = lstOfUser[0].Id;
        StrQuery = 'Select Id, Name, (SELECT id, LIHTC_Deal__c from External_Resources1__r where Role__c = \'CPA Firm\' and Resource_Name__c like \'%CohnReznick%\') From ENT_ED_LIHTC__c Where Project_Status__c = \'Closed Pmt Disbursed\' AND (NOT RecordType.Name like \'EMI%\') AND (FundStr__c != null) AND (NOT FundStr__c like \'%NYEF%\') AND (NOT FundStr__c like \'%EMOF%\') AND (NOT FundStr__c like \'%481%\') AND (NOT FundStr__c like \'%TDBUSA%\') AND (Asset_Manager__c != \'00530000004gy3s\')';
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(StrQuery);
    }

    global void execute(Database.BatchableContext BC, List<ENT_ED_LIHTC__c> scope)
    {
        // local variables to hold deal and entitiy id's while identifying deal's having missing documents.
        Set<Id> setofDealIds = new Set<Id>();
        Set<Id> dealsOfCohnReznick = new Set<Id>();
        Set<Id> setOfEntitiesForAnticipatedDate = new Set<Id>();
        Set<Id> setOfEntitiesDue = new Set<Id>();
        Set<Id> setOfEntitiesToExclude = new Set<Id>();

        // get deal ids
        for(ENT_ED_LIHTC__c deal: scope)
        {
            setofDealIds.add(deal.Id);
            for(ENT_External_Resource__c er :deal.External_Resources1__r)
            {
                dealsOfCohnReznick.add(er.LIHTC_Deal__c);
            }
        }

        for(ENT_Inbound_Correspondence_Entity__c entity: [Select Id, Name, Inbound_Correspondence__r.LIHTC_Deal__c From ENT_Inbound_Correspondence_Entity__c
                                                            where
                                                                ((Correspondence_Type__c = 'Draft Financial Statement' OR Correspondence_Type__c = 'Final Financial Statement') AND
                                                                Inbound_Correspondence__r.Review_Rating__c = 'C/Mini' and
                                                                Inbound_Correspondence__r.LIHTC_Deal__c in :dealsOfCohnReznick)
                                                            OR
                                                                (Anticipated_Received_Date__c >= TODAY and
                                                                (Correspondence_Type__c = 'Draft Financial Statement' OR Correspondence_Type__c = 'Draft Tax Return') and
                                                                Inbound_Correspondence__r.LIHTC_Deal__c in :setofDealIds)])
        {
            setOfEntitiesToExclude.add(entity.Id);
        }

        // find entities having due date before 1st March.
        Date DateOfFirstMarch = Date.newInstance(2020, 3, 1);

        for(ENT_Inbound_Correspondence_Entity__c entity : [select id, Inbound_Correspondence__r.LIHTC_Deal__c from ENT_Inbound_Correspondence_Entity__c where (Correspondence_Type__c = 'Draft Financial Statement' OR Correspondence_Type__c = 'Draft Tax Return') and (Deadline_Date__c < :DateOfFirstMarch) and Inbound_Correspondence__r.LIHTC_Deal__c in :setofDealIds ])
        {
            setOfEntitiesDue.add(entity.Id);
        }

        // Main list which identifies what is missing for recepient for the current reporting yaer(normally last year)
        String CurrentReportingYear = String.valueOf(System.Today().year() - 1);

        // Set to hold id of deal records.
        Set<Id> DealIdsToUpdate = new Set<Id>();
        for(ENT_Inbound_Correspondence_Entity__c entity: [SELECT Inbound_Correspondence__r.LIHTC_Deal__r.Deal_ID__c,Inbound_Correspondence__r.LIHTC_Deal__r.Partnership__r.Name, Id, Name, Correspondence_Type__c, Inbound_Correspondence__c,
                                                                Inbound_Correspondence__r.Name, Inbound_Correspondence__r.LIHTC_Deal__c
                                                            FROM
                                                                ENT_Inbound_Correspondence_Entity__c
                                                            WHERE
                                                                Reporting_Year__c = :CurrentReportingYear and
                                                                (Correspondence_Type__c = 'Draft Financial Statement' OR Correspondence_Type__c = 'Draft Tax Return') AND Not_Required__c = false AND
                                                                Date_Received__c = null and Inbound_Correspondence__r.LIHTC_Deal__c in :setofDealIds and
                                                                Id Not in :setOfEntitiesToExclude and
                                                                Id In :setOfEntitiesDue])
        {
            DealIdsToUpdate.add(entity.Inbound_Correspondence__r.LIHTC_Deal__c);
        }

        // list to hold deal records with assigned new flag(X12_31_Document_Missing__c) value.
        List<ENT_ED_LIHTC__c> lstDealsToUpdate = new List<ENT_ED_LIHTC__c>();

        for(ENT_ED_LIHTC__c deal : [Select Id, Name, X12_31_Document_Missing__c From ENT_ED_LIHTC__c Where Id in :DealIdsToUpdate])
        {
            //if(deal.X12_31_Document_Missing__c != True)
            //{
                deal.X12_31_Document_Missing__c = True;
                lstDealsToUpdate.add(deal);
            //}
        }

        try
        {
            // check if no deal records to update.
            if(!lstDealsToUpdate.isEmpty())
            {
                //Update lstDealsToUpdate;
                List<Database.SaveResult> lstDBSaveRsltDeals = Database.update(lstDealsToUpdate);
                lstDbSaveResult.addAll(lstDBSaveRsltDeals);
            }
        }
        catch (exception ex)
        {
            exception_String += ex.getStackTraceString() + '\n';
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        long NoOfUpdatedRecords = 0;
        // Iterate over all results and generate 'final_Result_String' to send email to notify user about failed records.
        for(Database.SaveResult singleresult : lstDbSaveResult)
        {
            // Check to make sure iterating over only unsuccess/failed results.
            if(singleresult.isSuccess() == false)
            {
                if(String.isBlank(final_Result_String))
            {
            final_Result_String = 'Please find detail of record(s) which failed during quarterly update' + '\n\n' + 'Record Id : ' + singleresult.getId() + ' Error : ' + singleresult.getErrors() + '\n';
            }
            else
            {
                final_Result_String += 'Record Id : ' + singleresult.getId() + ' Error : ' + singleresult.getErrors() + '\n';
            }
            }
            else
            {
                NoOfUpdatedRecords += 1;
                // Send single line in mail stating successful execution of batch.
                final_Result_String = 'Congratulations...!!! ' + NoOfUpdatedRecords + ' LIHTC Deal record(s) successfully updated.';
            }
        }

        // Define mail body and recipients.
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] {Label.ENT_Batch_LIHTC_Late_Notify_SetFlag_Owner};
        message.subject = 'Batch execution result of : ' + date.today().format() + ' : ENT_Batch_LIHTC_Late_Notify_SetFlag';
        String Str_exception_String = exception_String != null ? exception_String : '';
        message.plainTextBody = final_Result_String != null ? final_Result_String : 'No Records to update'   +  Str_exception_String ;
        message.setHtmlBody(final_Result_String != null ? final_Result_String : 'No Records to update'   +   Str_exception_String);
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};

        // Track Email Sent Result
        try
        {
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            if (results[0].success)
            {
                System.debug('The email was sent successfully.');
            }
            else
            {
                System.debug('The email failed to send: ' + results[0].errors[0].message);
            }
        }
        catch (Exception ex)
        {
            System.debug('The following exception has occurred: ' + ex.getMessage());
        }
    }

    // below method will schedule above batch with given cron expression and will be executed as a part of schedulable interface.
    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new ENT_Batch_LIHTC_Late_Notify_SetFlag());
    }
}