/**************************************************************
Class   : ENT_CTRL_LIHTC_Late_Notification
Author  : Bdarji
Date    : 31/01/2020
Details : Related ticket #EHCI-373 : 1231 Round 1 Draft Required late notice to FRC (End of Feb)
          This class helps to identify missing documents(entities) which falls under missing document criteria mentioned in ticket above.

          GetMissingDocumentsList - this method contains same logic implemented in batch (ENT_Batch_LIHTC_Late_Notify_SetFlag).
          logic is related to missing document(entity) list for deal, criteria for the missing document is mentioned on ticket EHCI-373.
          SINCE THIS IS AN URGENT FOR BUSINESS TO SEND A MISSING DOCUMENT NOTIFICATION TO RECIPIENTS, WE ARE DEPLOYING THIS AND WE WILL RE-VISIT THIS CODE AND OPTIMIZE.

Test Class : TEST_TestCoverage_EPIC14 (91%)
History : v1.0 - 31/01/2020 - Created
*****************************************************************/
public with sharing class ENT_CTRL_LIHTC_Late_Notification
{
    // public variables
    public Id PersonId {get;set;}
    public List<ENT_Inbound_Correspondence_Entity__c> LstEntities {get;set;}
    public Set<Id> SetofDealIds {get;set;}
    public Set<Id> DealsWithCohnReznick {get;set;}
    public Set<Id> SetOfEntitiesToExclude {get;set;}
    public Set<Id> SetOfEntitiesForAnticipatedDate {get;set;}
    public Set<Id> SetOfEntitiesDue {get;set;}
    public String OrganizationName {get;set;}
    public String AccountingManager {get;set;}
    public String AccountingManagerMailId {get;set;}
    public Map<String, List<ENT_Inbound_Correspondence_Entity__c>> MapOfDealWithMissingDocuments {get;set;}

    // get id of the recipient
    // we are retriving/getting below variable in vf component which will initialize method which gives us to list of missing entities.
    public Id RecipientId
    {
        get
        {
            RecipientId = PersonId;
            GetMissingDocumentsList(PersonId);
            return RecipientId;
        } set;
    }

    // constructor
    public ENT_CTRL_LIHTC_Late_Notification() {}

    // below method will find out missing document based on filter criteria.
    // filter criteria is mentioned on the ticket.
    public void GetMissingDocumentsList(Id PersonId)
    {
        // initialize variables
        SetofDealIds = new Set<Id>();
        DealsWithCohnReznick = new Set<Id>();
        SetOfEntitiesToExclude = new Set<Id>();
        SetOfEntitiesForAnticipatedDate = new Set<Id>();
        SetOfEntitiesDue = new Set<Id>();

        // get details of the external resource/recipient.
        // get 'Monica Spillane' user details.
        List<User> lstOfUser = [Select Id, Name From User Where Name=: Label.ENT_LIHTC_User_Monica_Spillane limit 1]; // 00530000004gy3s
        if(!lstOfUser.isEmpty())
        {
            for(ENT_External_Resource__c er: [SELECT Person__r.Contact__c,Account__r.Name, LIHTC_Deal__c FROM ENT_External_Resource__c WHERE Contact_Role__c = 'Financial Reporting Contact' AND primary__c = true AND Active__c = true AND
                                            LIHTC_Deal__r.Project_Status__c = 'Closed Pmt Disbursed' AND (NOT LIHTC_Deal__r.RecordType.Name like 'EMI%') AND (LIHTC_Deal__r.FundStr__c != null) AND
                                            (NOT LIHTC_Deal__r.FundStr__c like '%NYEF%') AND (NOT LIHTC_Deal__r.FundStr__c like '%EMOF%') AND (NOT LIHTC_Deal__r.FundStr__c like '%481%') AND
                                            (NOT LIHTC_Deal__r.FundStr__c like '%TDBUSA%') AND (LIHTC_Deal__r.Asset_Manager__c != :lstOfUser[0].Id) and Person__r.Contact__c =:PersonId])
            {
               SetofDealIds.add(er.LIHTC_Deal__c);
               if(OrganizationName == null)
               {
                   OrganizationName = er.Account__r.Name;
               }
            }
        }

        if(!SetofDealIds.isEmpty())
        {
            // get accounting manager details to show in the email.
            for(ENT_Enterprise_Team__c team: [Select Id, User__c, User__r.Name, User__r.Email From ENT_Enterprise_Team__c where Role__c = 'Accounting Manager' and Primary__c = true and Active__c = true and LIHTC_Deal__c in :SetofDealIds limit 1])
            {
                AccountingManager = team.User__r.Name;
                AccountingManagerMailId = team.User__r.Email;
            }
            // get external resource which deal has 'CohnReznick' as a CPA Firm role.
            for(ENT_External_Resource__c er: [select id, LIHTC_Deal__c, Primary__c, Active__c from ENT_External_Resource__c where Role__c = 'CPA Firm' and Primary__c = true and Active__c = true and Resource_Name__c like '%CohnReznick%' and LIHTC_Deal__c in :SetofDealIds])
            {
                DealsWithCohnReznick.add(er.LIHTC_Deal__c);
            }

            // get entities which has due date less then 3/1/2020
            Date DateOfFirstMarch = Date.newInstance(2020, 3, 1);
            for(ENT_Inbound_Correspondence_Entity__c entity : [select id, Inbound_Correspondence__r.LIHTC_Deal__c from ENT_Inbound_Correspondence_Entity__c where (Correspondence_Type__c = 'Draft Financial Statement' OR Correspondence_Type__c = 'Draft Tax Return') and (Deadline_Date__c < :DateOfFirstMarch) and Inbound_Correspondence__r.LIHTC_Deal__c in :SetofDealIds ])
            {
                SetOfEntitiesDue.add(entity.Id);
            }
        }

        for(ENT_Inbound_Correspondence_Entity__c entity: [Select Id, Name, Inbound_Correspondence__r.LIHTC_Deal__c From ENT_Inbound_Correspondence_Entity__c
                                                            where
                                                                ((Correspondence_Type__c = 'Draft Financial Statement' OR Correspondence_Type__c = 'Final Financial Statement') AND
                                                                Inbound_Correspondence__r.Review_Rating__c = 'C/Mini' and
                                                                Inbound_Correspondence__r.LIHTC_Deal__c in :DealsWithCohnReznick)
                                                            OR
                                                                (Anticipated_Received_Date__c >= TODAY and
                                                                (Correspondence_Type__c = 'Draft Financial Statement' OR Correspondence_Type__c = 'Draft Tax Return') and
                                                                Inbound_Correspondence__r.LIHTC_Deal__c in :setofDealIds)])
        {
            setOfEntitiesToExclude.add(entity.Id);
        }

        // Main list which identifies what is missing for recepient for the current reporting yaer(normally last year)
        String CurrentReportingYear = String.valueOf(System.Today().year() - 1);



        // Only if found missing entities then fill map.
        // below map will contain "dealId#Partnershipname" as key and Missing entities(ENT_Inbound_Correspondence_Entity__c) list as a vaue.
        // Key = Deal Id + Partnership Name
        // Value = List of 'ENT_Inbound_Correspondence_Entity__c'.
        MapOfDealWithMissingDocuments =  new Map<String, List<ENT_Inbound_Correspondence_Entity__c>>();
        for(ENT_Inbound_Correspondence_Entity__c entity: [SELECT Inbound_Correspondence__r.LIHTC_Deal__r.Deal_ID__c,Inbound_Correspondence__r.LIHTC_Deal__r.Partnership__r.Name, Id, Name, Correspondence_Type__c, Inbound_Correspondence__c,
                                                            Inbound_Correspondence__r.Name, Inbound_Correspondence__r.LIHTC_Deal__c, Inbound_Correspondence__r.Review_Rating__c
                                                        FROM
                                                            ENT_Inbound_Correspondence_Entity__c
                                                        WHERE
                                                            Reporting_Year__c = :CurrentReportingYear and
                                                            (Correspondence_Type__c = 'Draft Financial Statement' OR Correspondence_Type__c = 'Draft Tax Return') AND Not_Required__c = false AND
                                                            Date_Received__c = null and Inbound_Correspondence__r.LIHTC_Deal__c in :SetofDealIds and
                                                            Id Not in :SetOfEntitiesToExclude
                                                            and Id In :SetOfEntitiesDue
                                                        Order by Inbound_Correspondence__r.LIHTC_Deal__r.Partnership__r.Name ASC])
        {
            if(!MapOfDealWithMissingDocuments.containsKey(entity.Inbound_Correspondence__r.LIHTC_Deal__r.Deal_ID__c + '#' + entity.Inbound_Correspondence__r.LIHTC_Deal__r.Partnership__r.Name))
            {
                MapOfDealWithMissingDocuments.put(entity.Inbound_Correspondence__r.LIHTC_Deal__r.Deal_ID__c + '#' + entity.Inbound_Correspondence__r.LIHTC_Deal__r.Partnership__r.Name, new List<ENT_Inbound_Correspondence_Entity__c>{entity});
            }
            else
            {
                MapOfDealWithMissingDocuments.get(entity.Inbound_Correspondence__r.LIHTC_Deal__r.Deal_ID__c + '#' + entity.Inbound_Correspondence__r.LIHTC_Deal__r.Partnership__r.Name).add(entity);
            }
        }
    }
}