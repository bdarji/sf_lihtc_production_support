/***************************************************************
Class   : ENT_Ext_ReportingChecklist
Author  : NBanas
Date    : 06/03/2013
Details : Overrides the standard Reporting Checklist object
          view/edit/create pages to allow more precise control
          over the layout & the ability to create Review
          Checklists from the Entity Related List.
History : v1.0 - 06/03/2013 - Created (96% Code Coverage)
                              (TEST_TestCoverage_EPIC6)
          v1.1 - 03/04/2014 - Modified to incorporate Read-Only
                              Response Question Templates.
          v1.2 - 02/16/2014 - Modified to add TR review report link and link to AMFA
                              in reporting checklist page.
          v1.3 - 09/20/2019 - Bdarji - Adding Payment's list to show summary in vf page.
          v1.4 - 03/27/2020 - AMahajan - Adding Deal Id field in SOQL.
          v1.5 - 03/30/2020 - Bdarji - Changed Orderby Field as a part of Bi-Directional Issue(EHCI-1869) to make sure both interface 
                                       having consistency and showing/pointing same TRA checklist record.
                                     - Moved update statement to update entity(parent) next to insert call of child(checklsts) to avoid double checklist(child) creation.
*****************************************************************/
public with sharing class ENT_Ext_ReportingChecklist
{
    private Map<id, ENT_Inbound_Correspondence_Entity__c> MapEntities {get;set;}

    public boolean EditMode {get;set;}
    public ApexPages.StandardController ctrl {get;set;}
    public ENT_Inbound_Correspondence__c rec {get;set;}
    public Map<id, boolean> AllowChecklistCreation {get;set;}
    public Map<id, List<ENT_Review_Checklist__c>> ChecklistsByEntity {get;set;}
    public List<ENT_Review_Checklist__c> AMFAChecklists {get;set;}
    public integer AMFAChecklistsSize { get { return AMFAChecklists != null ? AMFAChecklists.size() : 0; } }
    public boolean CreateChecklistError {get;set;}
    public string CreateChecklistErrorMessage {get;set;}
    public id EntityID {get;set;}
    public boolean CanCreateTRAChecklists {get;set;}
    public boolean CanCreateAMFAChecklists {get;set;}
    public string SelectedAMFAChecklistPeriod {get;set;}
    public boolean AMFAChecklistsAllGenerated {get;set;}
    // v1.3 Starts
    public List<AggregateResult> PaymentsActualAg {get; set;}
    public Long totalAmt {get; set;}
    // v1.3 Ends
    
    public ENT_Ext_ReportingChecklist(ApexPages.StandardController controller)
    {
        ctrl = controller;
        initialize();
        EditMode = false;
    }
    
    private void initialize()
    {
        rec = (ENT_Inbound_Correspondence__c)ctrl.getRecord();
        
        CanCreateTRAChecklists = false;
        CanCreateAMFAChecklists = false;
        
        getAMFAPeriods();
        
        if(rec.Id != null)
        {
            //v1.4 - Added Deal_id__c field in SOQL for TR Review report link formulation
            rec = [select id, name,LIHTC_Deal__r.Name, LIHTC_Deal__r.Partnership__c, LIHTC_Deal__c, LIHTC_Deal__r.Project_Status__c, LIHTC_Deal__r.Deal_ID__c, Review_Rating__c, LIHTC_Deal__r.Asset_Manager__c,
                (select id, name,Deal_id__c, Correspondence_Type__c, Not_Required__c, Deadline_Date__c, Anticipated_Received_Date__c, Date_Received__c, Reviewer__c, Status__c, Status_Change_Date__c,
                    Inbound_Correspondence__r.Review_Rating__c, Inbound_Correspondence__r.Name, Inbound_Correspondence__c, Content_ID__c, Document__c,TR_Review_Report__c
                    from Inbound_Correspondence_Entities__r
                    order by Correspondence_Type__c)
                from ENT_Inbound_Correspondence__c where id = :rec.Id limit 1];
            //v1.4 END
                
            RecordType AMFART = [select Id, Name, sObjectType from RecordType where Name = 'Asset Management' and sObjectType = 'ENT_Review_Checklist__c' limit 1];
            
            AMFAChecklists = [select Id, Name, Checklist_Type__c, Related_Entity_Type__c, Period__c, Status_Formula__c, Estimated__c,Link_to_AMFA__c,
                CreatedById, CreatedBy.Name, LastModifiedById, LastModifiedBy.Name, CreatedDate, LastModifiedDate, Draft_Reviewer__c, Draft_Reviewer__r.Name
                from ENT_Review_Checklist__c where Reporting_Checklist__c = :rec.Id and RecordTypeId = :AMFART.Id
                order by Period__c];
            
            getAMFAPeriods();
        }
        else
            AMFAChecklists = new List<ENT_Review_Checklist__c>();
        
        List<RecordTypeInfo> availableRTS = ENT_Review_Checklist__c.SObjectType.getDescribe().getRecordTypeInfos();
        
        for(RecordTypeInfo rt : availableRTS)
        {
            if(rt.getName() == 'Asset Management')
            {
                CanCreateAMFAChecklists = rt.isAvailable();
            }
            else if(rt.getName()== 'Tax and Regional Accounting')
            {
                CanCreateTRAChecklists = rt.isAvailable();
            }
        }
        
        // Setup Necessary Checklist Creation Variables
        AllowChecklistCreation = new Map<id, boolean>();
        ChecklistsByEntity = new Map<id, List<ENT_Review_Checklist__c>>();
        MapEntities = new Map<id, ENT_Inbound_Correspondence_Entity__c>();
        set<id> eIDs = new set<id>();
        
        set<string> allowChecklistsOn = new set<string>();
        allowChecklistsOn.add('Quarter 1 Financial Statement');
        allowChecklistsOn.add('Quarter 2 Financial Statement');
        allowChecklistsOn.add('Quarter 3 Financial Statement');
        allowChecklistsOn.add('Quarter 4 Financial Statement');
        allowChecklistsOn.add('Draft Tax Return');
        allowChecklistsOn.add('Final Tax Return');
        allowChecklistsOn.add('Draft Financial Statement');
        allowChecklistsOn.add('Final Financial Statement');
        
        for(ENT_Inbound_Correspondence_Entity__c e : rec.Inbound_Correspondence_Entities__r)
        {
            AllowChecklistCreation.put(e.Id, false);
            ChecklistsByEntity.put(e.Id, new List<ENT_Review_Checklist__c>());
            eIDs.add(e.Id);
            MapEntities.put(e.Id, e);
        }
        
        // Get Checklists
        List<ENT_Review_Checklist__c> listChecklists = [SELECT Id, Name, Reporting_Checklist_Entity_Draft__c, Reporting_Checklist_Entity_Final__c, Back_To_Reviewer__c
            FROM ENT_Review_Checklist__c
            WHERE Reporting_Checklist_Entity_Draft__c IN :eIDs OR Reporting_Checklist_Entity_Final__c IN :eIDs
            // v1.5 Starts (Changed Order by Field)
            ORDER BY CreatedDate, LastModifiedDate DESC];
            // v1.5 Ends
        
        if(ChecklistsByEntity != null)
        {
            for(ENT_Review_Checklist__c cl : listChecklists)
            {
                if(cl.Reporting_Checklist_Entity_Draft__c != null)
                {
                    if(ChecklistsByEntity.get(cl.Reporting_Checklist_Entity_Draft__c) != null && ChecklistsByEntity.get(cl.Reporting_Checklist_Entity_Draft__c).size() == 0)
                        ChecklistsByEntity.get(cl.Reporting_Checklist_Entity_Draft__c).add(cl);
                }
                
                if(cl.Reporting_Checklist_Entity_Final__c != null)
                {
                    if(ChecklistsByEntity.get(cl.Reporting_Checklist_Entity_Final__c) != null && ChecklistsByEntity.get(cl.Reporting_Checklist_Entity_Final__c).size() == 0)
                        ChecklistsByEntity.get(cl.Reporting_Checklist_Entity_Final__c).add(cl);
                }
            }
        }
        
        Id pID = UserInfo.getProfileId();
        Profile uProfile = [select id, name from Profile where Id = :pId limit 1];
        
        // Setup Checklist Creation Ability and Disable Checklist Creation on Entities with Existing checklists
        for(ENT_Inbound_Correspondence_Entity__c e : rec.Inbound_Correspondence_Entities__r)
        {
            if(CanCreateTRAChecklists || uProfile.Name == 'System Administrator' || uProfile.Name == 'Super Admin User')
                AllowChecklistCreation.put(e.Id, allowChecklistsOn.contains(e.Correspondence_Type__c) && (ChecklistsByEntity.containsKey(e.Id) ? ChecklistsByEntity.get(e.Id).size() == 0 : false));
            else
                AllowChecklistCreation.put(e.Id, false);
        }
        
        EditMode = ApexPages.CurrentPage().getParameters().containsKey('retURL') && ENT_Review_Checklist__c.sObjectType.getDescribe().isUpdateable() ? true : false;
        
        CreateChecklistError = false;
        CreateChecklistErrorMessage = '';
        
        PaymentsActualAg = [Select Equity_Type__c, Benchmark__c, SUM(Amount__c)total, CALENDAR_YEAR(Payment_Date__c)year From ENT_LIHTC_Payment__c Where CALENDAR_YEAR(Payment_Date__c) =: Integer.valueOf(rec.Name) and RecordType.Name = 'Actual' And Payment_Summary__r.LIHTC_Deal__c =: rec.LIHTC_Deal__c group by Equity_Type__c, Benchmark__c, CALENDAR_YEAR(Payment_Date__c)];
        system.debug('--PaymentsActualAg--'+ PaymentsActualAg);
        totalAmt = 0;
        for(AggregateResult ar: PaymentsActualAg)
        {
            system.debug('--ar--'+ ar.get('total'));
            totalAmt += integer.valueOf(ar.get('total'));
        }
    }
    
    public PageReference CreateChecklist()
    {
        PageReference retval = null;
        CreateChecklistError = false;
        CreateChecklistErrorMessage = '';
        
        //v1.4 - Added Deal_id__c field in SOQL for TR Review report link formulation
        // Requery Map of Entities
        MapEntities = new Map<id, ENT_Inbound_Correspondence_Entity__c>([select id, name, Correspondence_Type__c, Not_Required__c, Deadline_Date__c, Content_ID__c, Document__c,
            Anticipated_Received_Date__c, Date_Received__c, Reviewer__c, Status__c, Status_Change_Date__c, Inbound_Correspondence__c, Inbound_Correspondence__r.Review_Rating__c,
            Inbound_Correspondence__r.LIHTC_Deal__c, Inbound_Correspondence__r.LIHTC_Deal__r.RecordTypeId, Additional_Attribute__c,Deal_ID__c,
            (select id, name from Review_Checklist_Draft__r),
            (select id, name from Review_Checklists_Final__r)
            from ENT_Inbound_Correspondence_Entity__c
            where Inbound_Correspondence__c = :rec.Id]);
        //v1.4 END
        
        // Determine Reporting Checklist Entity Counterpart (Draft -> Final, Final -> Draft, etc)
        if(EntityID != null)
        {
            if(MapEntities.containsKey(EntityID))
            {
                List<Map<ENT_Inbound_Correspondence_Entity__c, ENT_Inbound_Correspondence_Entity__c>> listChecklistCreates = new List<Map<ENT_Inbound_Correspondence_Entity__c, ENT_Inbound_Correspondence_Entity__c>>();
            
                ENT_Inbound_Correspondence_Entity__c priE = MapEntities.get(EntityID);
                
                if(priE.Review_Checklist_Draft__r.size() == 0 && priE.Review_Checklists_Final__r.size() == 0)
                {
                    ENT_Inbound_Correspondence_Entity__c secE = null;
                    
                    if(!priE.Correspondence_Type__c.contains('Quarter'))
                    {
                        for(ENT_Inbound_Correspondence_Entity__c e : MapEntities.values())
                        {
                            if((priE.Correspondence_Type__c.contains('Tax Return') && e.Correspondence_Type__c.contains('Tax Return') && !e.Correspondence_Type__c.contains('Amended')) ||
                                (priE.Correspondence_Type__c.contains('Financial Statement') && e.Correspondence_Type__c.contains('Financial Statement')))
                            {
                                if((priE.Correspondence_Type__c.contains('Draft') && e.Correspondence_Type__c.contains('Final')) ||
                                    (priE.Correspondence_Type__c.contains('Final') && e.Correspondence_Type__c.contains('Draft')))
                                {
                                    if(e.Review_Checklist_Draft__r.size() == 0 && e.Review_Checklists_Final__r.size() == 0)
                                    {
                                        secE = e;
                                    }
                                    else
                                    {
                                        CreateChecklistError = true;
                                        CreateChecklistErrorMessage = 'The specified entity already has a checklist associated.';
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    
                    if(!CreateChecklistError)
                    {
                        date priorRcvdDate = priE.Date_Received__c;
                        string priorStatus = priE.Status__c;
                        
                        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = true;
                        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
                        Utils.HasRun_RC_Creator = true;
                        priE.Date_Received__c = priE.Date_Received__c == null ? system.today() : priE.Date_Received__c;
                        priE.Status__c = (priE.Correspondence_Type__c.contains('Financial Statement') && !priE.Correspondence_Type__c.contains('Quarter')) || priE.Correspondence_Type__c.contains('Tax Return') ?
                            'Data Entry Complete' :
                            priE.Correspondence_Type__c.contains('Quarter') ?
                                'No DQ Issues' :
                                '';
                                
                         // v1.5 Starts
                         /*
                        - COMMENTED BELOW DML(line no. 252) AND MOVED (TO Line no. 265) AS A PART OF BI-DIRECTIONAL DOUBLE RECORD CREATION ISSUE FIX.
                        - Below 'Update' DML invokes 'ENT_Inbound_Correspondence_Entity_Create_CL' trigger, where on line no 376, its inserting its child(Review Checklist) records by method(ENT_Ext_ReviewChecklistTemplate.CreateReviewChecklist).
                        - And here also on line no 262, it's calling same method to insert child(Review Checklist) records.
                        - So, It was creating double child(checklist) records, so to avoid creation of such a extra/dummy records from both sides, we are updating parent(entity) record once child(checklist) records created.
                        - Since on the other side in entity trigger, there is a flag(checkListExists), which checks wether child(checklist) exists or not, it will not create child(Review Checklist) records again.
                        */
                        
                        // update priE;
                        // v1.5 Ends
                        
                        Map<ENT_Inbound_Correspondence_Entity__c, ENT_Inbound_Correspondence_Entity__c> tmp = new Map<ENT_Inbound_Correspondence_Entity__c, ENT_Inbound_Correspondence_Entity__c>();
                        tmp.put(priE, secE);
                        
                        listChecklistCreates.add(tmp);
                        
                        if(listChecklistCreates.size() > 0)
                        {
                            Database.SaveResult[] results = ENT_Ext_ReviewChecklistTemplate.CreateReviewChecklist(listChecklistCreates);
                           
                            // v1.5 Starts  (MOVED UPDATE STATEMENT FROM LINE NO#252 TO HERE, REASON ALREADY EXPLAINED)
                            update priE;
                            // v1.5 Ends
                            
                            if(results != null && results.size() > 0)
                            {
                                retval = new PageReference('/' + results[0].getID());
                            }
                            else
                            {
                                CreateChecklistError = true;
                                CreateChecklistErrorMessage = 'A checklist could not be located for the specified Review Rating and Entity Type.';
                                
                                // Reset the Entity back to the prior values
                                priE.Date_Received__c = priorRcvdDate;
                                priE.Status__c = priorStatus;
                                update priE;
                            }
                        }
                    }
                }
                else
                {
                    CreateChecklistError = true;
                    CreateChecklistErrorMessage = 'The specified entity already has a checklist associated.';
                }
            }
            else
            {
                CreateChecklistError = true;
                CreateChecklistErrorMessage = 'Entity ID could not be determined.  Please contact your System Administrator for assistance.';
            }
        }
       
        
        EntityID = null;
        
        return retval;
    }
    
    public PageReference CustomEdit()
    {
        PageReference retval = null;
        
        EditMode = true;
        
        return retval;
    }
    
    public PageReference CustomSave()
    {
        PageReference retval = null;
        
        retval = ctrl.save();
        
        EditMode = retval == null ? true : false;
        
        return retval;
    }
    
    public PageReference CustomCancel()
    {
        PageReference retval = null;
        
        if(!test.isRunningTest())
            ctrl.reset();
        
        initialize();
        
        EditMode = false;
        retval = ctrl.cancel();
        
        return retval;
    }
    
    public PageReference CreateAMFAChecklist()
    {
        PageReference retval = null;
        
        CreateChecklistError = false;
        
        if(SelectedAMFAChecklistPeriod != null && SelectedAMFAChecklistPeriod != '')
        {
            RecordType amfaRT = [select Id, Name, sObjectType from RecordType where sObjectType = 'ENT_Review_Checklist__c' and Name = 'Asset Management' limit 1];
        
            // Determine if Checklist Already Exists
            List<ENT_Review_Checklist__c> existingChecklists = [select id, Name, RecordTypeId, RecordType.Name, Period__c, Reporting_Checklist__c
                from ENT_Review_Checklist__c
                where Reporting_Checklist__c = :rec.Id and RecordTypeId = :amfaRT.Id and Period__c = :SelectedAMFAChecklistPeriod];
            
            // Create Checklist
            if(existingChecklists.size() == 0)
            {
                string periodType = SelectedAMFAChecklistPeriod.contains('Quarter') ? 'Quarterly' : SelectedAMFAChecklistPeriod.contains('Annual') ? 'Annual' : '';
                
                if(periodType != null && periodType != '')
                {
                    RecordType AMFACLTRT = [select Id, Name, sObjectType from RecordType where sObjectType = 'ENT_Review_Checklist_Template__c' and Name = 'Asset Management'];
                
                    ENT_Review_Checklist_Template__c RCLTemplate = null;
                    
                    try
                    {
                        RCLTemplate = [select id, Period__c, Name, 
                            Entity_Type__c, Automated_Via_Integration_Process__c, Uses_Approval_Process__c, RecordTypeId, RecordType.Name,
                            (SELECT Is_Multi_Select__c,Amount_Decimals_To_Display__c, Has_Comment__c, Has_Inconclusive_Value__c, Sub_List_Question_Values__c,
                                Has_N_A_Value__c, Name, Question_Field_Text__c, Question_Help_Text__c, Question_Sort_Order__c, Review_Checklist_Template__c, 
                                Type_of_Question__c, Section_Title__c, Sub_Section_Title__c, Automated_via_Integration_Process__c, Question_Template_ID__c,
                                Read_Only_Response__c
                                FROM Review_Checklist_Question_Templates__r 
                                WHERE active__c=true)
                            FROM ENT_Review_Checklist_Template__c 
                            WHERE RecordTypeId = :AMFACLTRT.Id and Active__c = true and Period__c = :periodType
                            limit 1];
                    }
                    catch(exception ex)
                    {
                        CreateChecklistError = true;
                        CreateChecklistErrorMessage = 'A checklist could not be located for the specified Period.';
                    }
                    
                    // Create Checklist
                    if(!CreateChecklistError)
                    {
                        // Begin Creating AMFA Checklist
                        if(RCLTemplate != null)
                        {
                            ENT_Review_Checklist__c newCL = new ENT_Review_Checklist__c(Name = RCLTemplate.Name, Reporting_Checklist__c = rec.Id,
                                Related_Entity_Type__c = RCLTemplate.Entity_Type__c, Type__c = (RCLTemplate.Period__c == 'Annually' ? 'Annual' : RCLTemplate.Period__c),
                                Uses_Approval_Process__c = RCLTemplate.Uses_Approval_Process__c, Parent_Checklist__c = null, Period__c = SelectedAMFAChecklistPeriod,
                                Automated_Via_Integration_Process__c = RCLTemplate.Automated_Via_Integration_Process__c,
                                RecordTypeId = AMFART.Id, Draft_Reviewer__c = rec.LIHTC_Deal__r.Asset_Manager__c);
                                
                            try
                            {
                                insert newCL;
                            }
                            catch(exception ex)
                            {
                                CreateChecklistError = true;
                                CreateChecklistErrorMessage = 'An error occurred while attempting to create the checklist.\nError: ' + ex.getMessage();
                            }
                            
                            if(!CreateChecklistError)
                            {
                                // Define Review Checklist Questions from Template
                                List<ENT_Review_Checklist_Question__c> tQuestions = new List<ENT_Review_Checklist_Question__c>();
                                for(ENT_Review_Checklist_Question_Template__c qt : RCLTemplate.Review_Checklist_Question_Templates__r)
                                {
                                    ENT_Review_Checklist_Question__c rec = new ENT_Review_Checklist_Question__c(Has_Comment__c = qt.Has_Comment__c, Has_Inconclusive_Value__c = qt.Has_Inconclusive_Value__c,
                                        Has_N_A_Value__c = qt.Has_N_A_Value__c, Is_Multi_Select__c = qt.Is_Multi_Select__c, Question_Field_Text__c = qt.Question_Field_Text__c, Question_Help_Text__c = qt.Question_Help_Text__c,
                                        Question_Sort_Order__c = qt.Question_Sort_Order__c, Section_Title__c = qt.Section_Title__c, Picklist_Values__c = qt.Sub_List_Question_Values__c,
                                        Question_Field_Value__c = null, Automated_via_Integration_Process__c = qt.Automated_via_Integration_Process__c, Question_Template_ID__c = qt.Question_Template_ID__c,
                                        Sub_Section_Title__c = qt.Sub_Section_Title__c, Type_of_Question__c = qt.Type_of_Question__c, Amount_Field_Value__c = null, Review_Checklist__c = newCL.Id,
                                        Read_Only_Response__c = qt.Read_Only_Response__c);
                                    
                                    tQuestions.add(rec);
                                }
                                
                                insert tQuestions;
                                
                                retval = new PageReference('/' + newCL.Id);
                            }
                        }
                    }
                }
                else
                {
                    CreateChecklistError = true;
                    CreateChecklistErrorMessage = 'An unknown error occurred while attempting to determine the appropriate Checklist Template. Please contact your System Administrator for assistance.';
                }
            }
            else
            {
                CreateChecklistError = true;
                CreateChecklistErrorMessage = 'A checklist already exists for the specified period [' + SelectedAMFAChecklistPeriod + '].';
            }
        }
        else
        {
            CreateChecklistError = true;
            CreateChecklistErrorMessage = 'You must select a what period you want to create an estimate for.';
        }
        //SelectedAMFAChecklistPeriod
        
        return retval;
    }
    
    public List<SelectOption> getAMFAPeriods()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        if(AMFAChecklists == null || AMFAChecklists.size() == 0)
        {
            options.add(new SelectOption('','--None--'));
            options.add(new SelectOption('Annual','Annual'));
            options.add(new SelectOption('Quarter 1','Quarter 1'));
            options.add(new SelectOption('Quarter 2','Quarter 2'));
            options.add(new SelectOption('Quarter 3','Quarter 3'));
            //options.add(new SelectOption('Quarter 4','Quarter 4'));
        }
        else
        {
            boolean hasAnnual = false;
            boolean hasQ1 = false;
            boolean hasQ2 = false;
            boolean hasQ3 = false;
            //boolean hasQ4 = false;
        
            for(ENT_Review_Checklist__c cl : AMFAChecklists)
            {
                if(cl.Period__c == 'Annual')
                {
                    hasAnnual = true;
                }
                else if(cl.Period__c == 'Quarter 1')
                {
                    hasQ1 = true;
                }
                else if(cl.Period__c == 'Quarter 2')
                {
                    hasQ2 = true;
                }
                else if(cl.Period__c == 'Quarter 3')
                {
                    hasQ3 = true;
                }
                /*else if(cl.Period__c == 'Quarter 4')
                {
                    hasQ4 = true;
                }*/
            }
            
            options.add(new SelectOption('','--None--'));
            
            if(!hasAnnual)
            {
                options.add(new SelectOption('Annual','Annual'));
            }
            
            if(!hasQ1)
            {
                options.add(new SelectOption('Quarter 1','Quarter 1'));
            }
            
            if(!hasQ2)
            {
                options.add(new SelectOption('Quarter 2','Quarter 2'));
            }
            
            if(!hasQ3)
            {
                options.add(new SelectOption('Quarter 3','Quarter 3'));
            }
            
            /*if(!hasQ4)
            {
                options.add(new SelectOption('Quarter 4','Quarter 4'));
            }*/
            
            AMFAChecklistsAllGenerated = hasAnnual && hasQ1 && hasQ2 && hasQ3 /*&& hasQ4*/ ? true : false;
        }
        
        return options;
    }
}