/***************************************************************
Class   : TEST_TestCoverage_EPIC14
Author  : Bdarji
Date    : 02/25/2020
Details : This class provides test coverage for various Apex
          Classes.
          Classes Covered Are:
          -  ENT_CTRL_LIHTC_Late_Notification (78%)
          -  ENT_Batch_LIHTC_Late_Notify_SetFlag (78%)
History - 02/25/2020 - Bdarji - created
***************************************************************/
@isTest(SeeAllData = false)
public class TEST_TestCoverage_EPIC14
{

    // below method will create a test data required in test methods, to avoid creating test data in every test method.
    @testSetup static void setup() {
        // get record types.
        RecordType acctRT = [SELECT Id, Name, SobjectType, IsActive FROM RecordType where SobjectType = 'Account' and Name = 'Standard'];
        RecordType teamOppRT = [SELECT Id, Name, SobjectType, IsActive FROM RecordType where SobjectType = 'ENT_External_Resource__c' and Name = 'LIHTC Deal External Resource'];

        // create project
        ENT_Project__c testP = new ENT_Project__c(Name='Test P');
        insert testP;

        // get record type for deal
        RecordType dRT = [select id from RecordType where SobjectType = 'ENT_ED_LIHTC__c' and name = 'Standard Deal' limit 1];

        // create deal
        ENT_ED_LIHTC__c testD = new ENT_ED_LIHTC__c(Name = 'Test D', Project__c = testP.Id, RecordTypeId = dRT.Id, Project_Status__c = 'Closed Pmt Disbursed', FundStr__c = 'dummyfundstring');
        insert testD;

        // start
        List<ENT_Inbound_Correspondence__c> lstRepChklst = [select Id, name, LIHTC_Deal__c from ENT_Inbound_Correspondence__c];
        lstRepChklst[0].Name = '2019';
        update lstRepChklst;

        List<ENT_Inbound_Correspondence__c> lstIc = [select id, name,Review_Rating__c,LIHTC_Deal__c from ENT_Inbound_Correspondence__c where LIHTC_Deal__c = :lstRepChklst[0].LIHTC_Deal__c];
        for(ENT_Inbound_Correspondence__c ic: lstIc)
        {
            ic.Review_Rating__c = 'C/Mini';
        }
        update lstIc;
        // end

        // get and assign 'Anticipated_Received_Date__c' value as required in entities.
        List<ENT_Inbound_Correspondence_Entity__c> lstAllEntity = [Select id, name, Anticipated_Received_Date__c, Deadline_Date__c, Correspondence_Type__c, Reporting_Year__c, Not_Required__c from ENT_Inbound_Correspondence_Entity__c limit 5];
        for(ENT_Inbound_Correspondence_Entity__c entity: lstAllEntity)
        {
            entity.Not_Required__c = false;
            entity.Date_Received__c = null;
            entity.Anticipated_Received_Date__c = system.today().adddays(1);
            entity.Correspondence_Type__c = 'Draft Financial Statement';
        }
        // update entities to set values as required.
        update lstAllEntity;

        // take records other then above.
        Date DateABC = system.today().adddays(1);
        List<ENT_Inbound_Correspondence_Entity__c> lstAllEntity1 = [Select id, name, Anticipated_Received_Date__c, Deadline_Date__c, Correspondence_Type__c, Reporting_Year__c, Not_Required__c from ENT_Inbound_Correspondence_Entity__c where Anticipated_Received_Date__c != :DateABC LIMIT 5];
        for(ENT_Inbound_Correspondence_Entity__c entity: lstAllEntity1)
        {
            entity.Not_Required__c = false;
            entity.Date_Received__c = null;
            entity.Correspondence_Type__c = 'Draft Financial Statement';
            Date DateBeforeOfFirstMarch = Date.newInstance(2020, 1, 1);
            entity.Deadline_Date__c = DateBeforeOfFirstMarch;
        }
        // update entities to set values as required.
        update lstAllEntity1;

        /*List<ENT_Inbound_Correspondence__c> lstRepChklst = [select Id, name, LIHTC_Deal__c from ENT_Inbound_Correspondence__c];
        lstRepChklst[0].Name = '2019';
        update lstRepChklst;

        List<ENT_Inbound_Correspondence__c> lstIc = [select id, name,Review_Rating__c,LIHTC_Deal__c from ENT_Inbound_Correspondence__c where LIHTC_Deal__c = :lstRepChklst[0].LIHTC_Deal__c];
        for(ENT_Inbound_Correspondence__c ic: lstIc)
        {
            ic.Review_Rating__c = 'C/Mini';
        }
        update lstIc;*/

        // create account
        Account testAcct = new Account(Name = 'Test Account ER Batch', RecordTypeId = acctRT.Id);
        insert testAcct;

        // create contact
        Contact testContact = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = testAcct.Id);
        insert testContact;

        // create business relationship for FRC external resource.
        ENT_Business_Relationship__c testRelationship = [select id, name, account__c, contact__c, contact__r.firstname, contact__r.lastname from ENT_Business_Relationship__c where Contact__c = :testContact.Id limit 1];

        // create active and primary FRC external resource
        ENT_External_Resource__c er = new ENT_External_Resource__c();
        er.Account__c = testAcct.Id;
        er.Person__c = testRelationship.Id;
        er.primary__c=true;
        er.Contact_Role__c = 'Financial Reporting Contact';
        er.RecordTypeId = teamOppRT.Id;
        er.LIHTC_Deal__c = lstRepChklst[0].LIHTC_Deal__c;
        er.Role__c = 'Test Role';
        er.Active__c = true;
        er.Start_Date__c = system.today().addDays(-7);
        Insert er;

        // create account
        Account testAcctCohnReznick = new Account(Name = ' CohnReznick LLP (New York)', RecordTypeId = acctRT.Id);
        insert testAcctCohnReznick;

        // create contact
        Contact testContactCohnReznick = new Contact(FirstName = 'Cohn', LastName = 'Reznick', AccountId = testAcctCohnReznick.Id);
        insert testContactCohnReznick;

        // create business relationship for FRC external resource.
        ENT_Business_Relationship__c testRelationshipCohnReznick = [select id, name, account__c, contact__c, contact__r.firstname, contact__r.lastname from ENT_Business_Relationship__c where Contact__c = :testContactCohnReznick.Id limit 1];

        // create active and primary FRC external resource
        ENT_External_Resource__c er1 = new ENT_External_Resource__c();
        er1.Account__c = testAcctCohnReznick.Id;
        er1.Person__c = testRelationshipCohnReznick.Id;
        er1.primary__c=true;
        er1.Contact_Role__c = 'Financial Reporting Contact';
        er1.RecordTypeId = teamOppRT.Id;
        er1.LIHTC_Deal__c = lstRepChklst[0].LIHTC_Deal__c;
        er1.Role__c = 'CPA Firm';
        er1.Active__c = true;
        er1.Start_Date__c = system.today().addDays(-7);
        Insert er1;

        // create enterprise team from 'ENT_EPIC_TestUtil' class for above deal.
        List<ENT_Enterprise_Team__c> lstEnterpriseTeams = ENT_EPIC_TestUtil.CreateTestEnterpriseTeam();

        // update deal id to set as required.
        for(ENT_Enterprise_Team__c team: lstEnterpriseTeams)
        {
            team.LIHTC_Deal__c = lstRepChklst[0].LIHTC_Deal__c;
        }
        lstEnterpriseTeams[0].Role__c = 'Accounting Manager';

        // update deal id in all created enterprise team.
        update lstEnterpriseTeams;
    }

    // below method will test logic implemented in batch class.
    static testMethod void Test_ENT_CTRL_LIHTC_Late_Notification()
    {
        // get contact list created from test method.
        List<Contact> lstContact = [Select Id, Name, FirstName, LastName, AccountId  From Contact];

        Test.StartTest();
        ENT_CTRL_LIHTC_Late_Notification ctrl = new ENT_CTRL_LIHTC_Late_Notification();

        // Setting below variable calls 'GetMissingDocumentsList' method which contains SOQL statements.
        ctrl.PersonId = lstContact[0].Id;
        Test.StopTest();

        // Get a string which accepts 'PersonIdInsideClass' variable in vf component.
        Id aString = ctrl.PersonIdInsideClass;

        // Check assert, to verify the same person/recipient Id is assigned/passed to controller of vf component, to generate an email content.
        system.assertEquals(ctrl.PersonId, lstContact[0].Id);
    }

    static testMethod void Test_ENT_Batch_LIHTC_Late_Notify_SetFlag()
    {
        Test.startTest();
        // below batch will identify deals and update 'X12_31_Document_Missing__c' (checkbox) flag true.
        // X12_31_Document_Missing__c flag true indicates that deal has missing document according to '12/31 FRC draft required late notification' criteria.
        ENT_Batch_LIHTC_Late_Notify_SetFlag btch = new ENT_Batch_LIHTC_Late_Notify_SetFlag();
        DataBase.executeBatch(btch);
        Test.stopTest();
    }
    
    // Below method will test logic/functionality written in 'ENT_Batch_LIHTC_Late_Notify_SetFlag' class.
    public static Testmethod void Test_ENT_Batch_LIHTC_Late_Notify_SetFlag_Scheduler()
    {
        // Define cron expression same as used in batch.
        String CRON_EXP = '0 0 0 1 JAN,APR,JUL,OCT ?';
    
        Test.startTest();
        Id jobId = System.schedule('TestSetFlag ENT_Batch_LIHTC_Late_Notify_SetFlag', CRON_EXP, new ENT_Batch_LIHTC_Late_Notify_SetFlag());
        Test.stopTest();
    
        // Verify job is scheduled with same cron expression.
        List<CronTrigger> ct = [SELECT Id, CronJobDetailId, NextFireTime, PreviousFireTime, StartTime, CronExpression, CreatedDate, TimesTriggered FROM CronTrigger where CronExpression = '0 0 0 1 JAN,APR,JUL,OCT ?' order by CreatedDate desc limit 1];
        System.assertEquals(ct[0].Id, jobId);
    }
}