/***************************************************************
  Class   : ENT_FundTriggerHandler
  Author  : NDethe
  Date    : 08/08/2016
  Details : This is global Fund Object trigger handler class which contains all Fund trigger code related to the Fund Object functinonality.
  Coverage Classes:

  History : v1.0 - 08/08/2016 - As part of Code Optimization project, created this new trigger handler by optimizing and merging code of "ENT_PopulateFundAbbreviationValidation_Trigger", "ENT_Fund_UpdateInsInfoFundAlloc" and "ENT_Fund_PopTotalTrancheAmt" triggers into single this class.
            v1.1 - Ashukla -  06/07/2018 - On Cretion of Fund, create a fund closing checklist,  entity.
            v1.2 - 05/05/2020 - Bdarji - Related to #EHCI-1980, Added 'DQ_Indicator_Evaluate__c' check to make it true only if its false, to avoid updating unnecesary 'Fund Allocation' records and hence 'CPU timeout'
***************************************************************/
public class ENT_FundTriggerHandler
{
    public static Map<String, Id> mapAllRecTyp = new Map<String, Id> ();

    public static Map<String, Id> mapRecordTypes
    {
        get
        {
            if (mapAllRecTyp.isEmpty())
            mapAllRecTyp = ENT_Utils.GetMapRecordType('ENT_Fund__c');
            return mapAllRecTyp;
        }
    }


    //This method is created from the Before part code of the "ENT_PopulateFundAbbreviationValidation_Trigger" trigger for applying the Fund Abbreviation validations.
    public static void ApplyFundAbbreviationValidations(List<ENT_Fund__c> lstNewFund)
    {
        for (ENT_Fund__c FundsUpdate : lstNewFund)
        {
            if (FundsUpdate.Record_Type_Name__c == 'Fund')
            {
                if (FundsUpdate.Fund_Abbreviation_Validation__c != FundsUpdate.Name)
                {
                    FundsUpdate.Fund_Abbreviation_Validation__c = FundsUpdate.Name;
                }
            }
            else if (FundsUpdate.RecordTypeId == mapRecordTypes.get('Tranche'))
            {
                if (FundsUpdate.Tranche_Abbreviation_Validation__c != FundsUpdate.Name)
                {
                    FundsUpdate.Tranche_Abbreviation_Validation__c = FundsUpdate.Name;
                }
            }
        }
    }

    //This method is created from the after insert and after update part code of the "ENT_PopulateFundAbbreviationValidation_Trigger" trigger for checking duplicate value of the Fund and Fund child tranche records.
    public static void ValidateDuplicateOnFundAndTrancheRecord(List<ENT_Fund__c> lstInsUpdFunds)
    {
        Set<Id> BeforeFundIds = new Set<Id> ();
        Map<Id, ENT_Fund__c> FundInfoMap = new Map<Id, ENT_Fund__c> ();
        for (ENT_Fund__c Fund : lstInsUpdFunds)
        {
            if (Fund.RecordTypeId == mapRecordTypes.get('Tranche'))
            {
                BeforeFundIds.add(Fund.Tranche_Parent_Fund__c);
            }
        }

        FundInfoMap = new map<Id, ENT_Fund__c> ([select name, Id, Tranche_Parent_Fund__c, Record_Type_Name__c, (select id, name from fund_child_tranches__r) from ENT_Fund__c where Id in :BeforeFundIds]);

        for (ENT_Fund__c FundsUpdate : lstInsUpdFunds)
        {
            if (FundsUpdate.RecordTypeId == mapRecordTypes.get('Tranche'))
            {
                if (FundInfoMap.containskey(FundsUpdate.Tranche_Parent_Fund__c))
                {
                    ENT_Fund__c FundMapValue = FundInfoMap.get(FundsUpdate.Tranche_Parent_Fund__c);

                    for (ENT_Fund__c t : FundMapValue.fund_child_tranches__r)
                    {
                        //system.debug('###Tranche value ' + t.Id + '     ' + t.Name);
                        if (FundsUpdate.Id != t.id && FundsUpdate.Name == t.Name)
                        {
                            FundsUpdate.addError('Duplicate value for record : [ ' + '<a href=\'/' + t.Id + '\'>' + t.Name + '</a>' + ' ]');
                        }
                    }
                }
            }
        }
    }

    //This method is created from the after update part code of the "ENT_PopulateFundAbbreviationValidation_Trigger" trigger for updaing the Fund Allocation related to Fund.
    public static void UpdateFundAllocations(list<ENT_Fund__c> lstNewFunds, map<Id, ENT_Fund__c> mapOldFunds)
    {
        //system.debug('lstNewFunds >>>> ' + lstNewFunds);
        //system.debug('mapOldFunds >>>> ' + mapOldFunds);
        set<Id> FundIds = new set<Id> ();
        map<Id, ENT_Fund_Allocation__c> UpdateFundAllocation = new map<Id, ENT_Fund_Allocation__c> ();
        for (ENT_Fund__c FundId : lstNewFunds)
        {
            fundIds.add(FundId.Id);
        }

        map<Id, ENT_Fund__c> FundAllocmap = new map<Id, ENT_Fund__c> ([SELECT e.Name, e.Investor_Consent_Required__c, e.Id, (SELECT Id, Fund_Legal_Name__c, Fund__c, LIHTC_Deal__c, Investor_Consent_Required__c FROM Fund_Allocations__r) FROM ENT_Fund__c e WHERE e.Id in :FundIds]);
        //system.debug('FundAllocmap >>>> ' + FundAllocmap);
        for (ENT_Fund__c Fund : lstNewFunds)
        {
            ENT_Fund__c FundAllocations = FundAllocmap.get(Fund.Id);
            //system.debug('FundAllocations >>>> ' + FundAllocations);
            //system.debug('fund allocation size is ' + FundAllocations.Fund_Allocations__r.size());
            //system.debug('Old Investor Consent value is ' + trigger.oldMap.get(Fund.Id).Investor_Consent_Required__c);
            //system.debug('New Investor Consent value is ' + Fund.Investor_Consent_Required__c);

            if (Fund.Investor_Consent_Required__c != mapOldFunds.get(Fund.Id).Investor_Consent_Required__c)
            {
                //system.debug('>>>> Investor_Consent_Required__c value changed');
                if (FundAllocations.Fund_Allocations__r.size() > 0)
                {
                    //system.debug('FundAllocations.Fund_Allocations__r >>>> ' + FundAllocations.Fund_Allocations__r);
                    for (ENT_Fund_Allocation__c FundAlloc : FundAllocations.Fund_Allocations__r)
                    {
                        if (!UpdateFundAllocation.containskey(FundAlloc.Id))
                        {
                            UpdateFundAllocation.put(FundAlloc.Id, new ENT_Fund_Allocation__c(Id = FundAlloc.Id, Fund__c = Fund.Id));
                        }
                    }
                }
            }
        }
        //system.debug('UpdateFundAllocation >>>> ' + UpdateFundAllocation);
        if (UpdateFundAllocation.size() > 0)
        {
            UPDATE UpdateFundAllocation.values();
        }
    }

    //This trigger updates the Fund Insurance Financial Size Category, Fund Insurance Carrier Rating and Financial Reporting Due Date on the Fund Allocation 
    //when these values are updated on the Fund.
    //v1.1 - 06/04/2013 - Modified the code and added the condition to check
    //                    if the Financial Reporting Due Date has changed on 
    //                    the fund so that all the fund allocation records are 
    //                    updated. 
    //v1.2 - 10/05/2014 - While updating fund allocation also update DQ_Indicator_Evaluate__c to true.
    public static void UpdateFundInsuInfoFundAllocations(list<ENT_Fund__c> lstNewFunds, map<Id, ENT_Fund__c> mapOldFunds)
    {
        set<id> fIDs = new set<id> ();
        map<Id, ENT_Fund_Allocation__c> UpdateFundAllocation = new map<Id, ENT_Fund_Allocation__c> ();
        Id fID = (mapRecordTypes.containsKey('Fund')) ? mapRecordTypes.get('Fund') : null;

        for (ENT_Fund__c fnd : lstNewFunds)
        {
            if (fnd.RecordTypeId == fID)
            {
                if (fnd.Id != null)
                {
                    ENT_Fund__c OldFundInfo = trigger.isUpdate ? mapOldFunds.get(fnd.Id) : null;

                    if (OldFundInfo != null)
                    {
                        if (fnd.Required_Fund_Insurance_Rating__c != OldFundInfo.Required_Fund_Insurance_Rating__c || fnd.Required_Fund_Insurance_Fin_Size_Catego__c != OldFundInfo.Required_Fund_Insurance_Fin_Size_Catego__c || fnd.Financial_Reporting_Due_Date__c != OldFundInfo.Financial_Reporting_Due_Date__c)
                        {
                            fIDs.add(fnd.Id);
                        }
                    }
                    else
                    {
                        fIDs.add(fnd.Id);
                    }
                }
            }
        }

        if (fIDs.size() > 0)
        {
            // moved into FOR loop for making use of SOQL For loop over large data volume.
            /*Map<id, ENT_Fund__c> fundAllocMap = new Map<id, ENT_Fund__c> ([SELECT id, name, Required_Fund_Insurance_Rating__c, Required_Fund_Insurance_Fin_Size_Catego__c, Financial_Reporting_Due_Date__c,
            (SELECT id, name, Financial_Reporting_Due_Date__c
             FROM Fund_Allocations__r)
                                                                          FROM ENT_Fund__c
                                                                          WHERE Id in :fIDs]);

            for (ENT_Fund__c fnd : fundAllocMap.values())*/
            // v1.2 - Start/End - Added field in query.
            for(ENT_Fund__c fnd : [SELECT id, name, Required_Fund_Insurance_Rating__c, Required_Fund_Insurance_Fin_Size_Catego__c, Financial_Reporting_Due_Date__c, (SELECT id, name, Financial_Reporting_Due_Date__c, DQ_Indicator_Evaluate__c
             FROM Fund_Allocations__r) FROM ENT_Fund__c WHERE Id in :fIDs])
            {
                for (ENT_Fund_Allocation__c fndAlloc : fnd.Fund_Allocations__r)
                {
                    // v1.2 Start/End (Added 'DQ_Indicator_Evaluate__c' check to make it true only if its false, to avoid updating unnecesary 'Fund Allocation' records and hence 'CPU timeout')
                    if (!UpdateFundAllocation.containskey(fndAlloc.Id) && fndAlloc.DQ_Indicator_Evaluate__c == false)
                    {
                        UpdateFundAllocation.put(fndAlloc.Id, new ENT_Fund_Allocation__c(Id = fndAlloc.Id, DQ_Indicator_Evaluate__c = true));
                    }
                }
            }

            //system.debug('### The UpdatFundAllocation size is : ' + updateFundAllocation);
            if (UpdateFundAllocation.size() > 0)
            {
                UPDATE UpdateFundAllocation.values();
            }
        }
    }

    // v1.3 - 20/05/2016 - (VVaishnav) While udating fund's primary fund analyst, capital call's primary fund analyst should also get updated.
    public static void UpdateCapitalCallsPrimryFundAnalyst(list<ENT_Fund__c> lstNewFunds, map<Id, ENT_Fund__c> mapOldFunds)
    {
        Map<Id, ENT_Fund__c> mapOfChangedIds = new Map<Id, ENT_Fund__c> ();
        Set<Id> setOfUserIds = new Set<Id> ();
        Map<Id, User> mapOfUsers = new Map<Id, User> ();
        for (ENT_Fund__c fundRec : lstNewFunds)
        {
            ENT_Fund__c OldFundInfo = trigger.isUpdate ? mapOldFunds.get(fundRec.Id) : null;

            if (OldFundInfo != null)
            {
                if (fundRec.Primary_Fund_Analyst__c != OldFundInfo.Primary_Fund_Analyst__c)
                {
                    mapOfChangedIds.put(fundRec.Id, fundRec);
                    setOfUserIds.add(fundRec.Primary_Fund_Analyst__c);
                }
            }
        }

        if (!setOfUserIds.isEmpty())
        {
            for (User userRec :[SELECT Id, Email from User WHERE ID IN :setOfUserIds])
            {
                mapOfUsers.put(userRec.Id, userRec);
            }
        }

        if (!mapOfChangedIds.isEmpty())
        {
            // Only update when the field has the wrong value
            List<ENT_Capital_Call__c> lstToUpdate = new List<ENT_Capital_Call__c> ();

            for (ENT_Capital_Call__c capitalCallRec :[SELECT Id, Fund__c, Primary_Fund_Analyst__c
                 FROM ENT_Capital_Call__c
                 WHERE Fund__c IN :mapOfChangedIds.keySet()])
            {
                ENT_Fund__c fundRec = mapOfChangedIds.get(capitalCallRec.Fund__c);
                capitalCallRec.Primary_Fund_Analyst__c = mapOfUsers.containskey(fundRec.Primary_Fund_Analyst__c) ? mapOfUsers.get(fundRec.Primary_Fund_Analyst__c).Email : null;
                lstToUpdate.add(capitalCallRec);

            }

            if (!lstToUpdate.isEmpty())
            {
                UPDATE lstToUpdate;
            }
        }
    }

    //v1.0 - 05/18/2012 : NBanas: Updates Total Tranche Amount on parent Funds.
    public static void UpdateParentFundTotalTrancheAmt(list<ENT_Fund__c> lstNewFunds)
    {
        set<id> fIDs = new set<id> ();
        Id tID = (mapRecordTypes.containsKey('Tranche')) ? mapRecordTypes.get('Tranche') : null;

        for (ENT_Fund__c fnd : lstNewFunds)
        {
            if (fnd.RecordTypeId == tID)
            {
                fIDs.add(fnd.Tranche_Parent_Fund__c);
            }
        }

        if (fIDs.size() > 0)
        {
            // moved into FOR loop for making use of SOQL For loop over large data volume.
            /*Map<id, ENT_Fund__c> fundsMap = new Map<id, ENT_Fund__c> ([SELECT id, name, Total_Tranche_Amount__c,
                                                                        (SELECT id, name, Tranche_Amount__c
                                                                         FROM Fund_Child_Tranches__r)
                                                                      FROM ENT_Fund__c
                                                                      WHERE id in :fIDs]);

            for (ENT_Fund__c fnd : fundsMap.values()) */
            List<ENT_Fund__c> lstfunds = new List<ENT_Fund__c> ();
            for(ENT_Fund__c fnd : [SELECT id, name, Total_Tranche_Amount__c,
                                                                        (SELECT id, name, Tranche_Amount__c
                                                                         FROM Fund_Child_Tranches__r)
                                                                      FROM ENT_Fund__c
                                                                      WHERE id in :fIDs])
            {
                fnd.Total_Tranche_Amount__c = 0;

                for (ENT_Fund__c trnch : fnd.Fund_Child_Tranches__r)
                {
                    fnd.Total_Tranche_Amount__c += trnch.Tranche_Amount__c != null ? trnch.Tranche_Amount__c : 0;
                }
                lstfunds.add(fnd);
            }
            if(!lstfunds.isEmpty())
                UPDATE lstfunds;
        }
    }
    
    //v1.1 Starts
    // Code Covered in EPIC8 Test Class.
    public static void CreateFundClosingOnFundCreation(List<ENT_Fund__c> lstNewFund)
    {
        List<ENT_LIHTC_Fund_Reporting_Checklist__c> lstFundReportingChecklist = new List<ENT_LIHTC_Fund_Reporting_Checklist__c>();
        for(ENT_Fund__c fundToIterate :lstNewFund)
        {
            lstFundReportingChecklist.add(new ENT_LIHTC_Fund_Reporting_Checklist__c(Name='Fund Closing', Fund__c=fundToiterate.Id));
        }
        if(!lstFundReportingChecklist.IsEmpty())
        {
            INSERT lstFundReportingChecklist;
        }         
    }
    //v1.1 Ends
}