/**************************************************************
Class   : ENT_CTRL_LIHTC_Late_Notification_R3
Author  : Bdarji
Date    : 04/15/2020
Details : Related ticket #EHCI-348
Test Class : TEST_TestCoverage_EPIC15 - 100%
History : v1.0 - 4/15/2020 - Bdarji - Created
*****************************************************************/
public with sharing class ENT_CTRL_LIHTC_Late_Notification_R3
{
    // public variables
    public Id PersonId {get;set;}
    public Map<string, List<String>> MapOfDealWithMissingEntitiesRound3 {get;set;}
    public String AccountingManager {get;set;}
    public String AccountingManagerMailId {get;set;}
    public String OrganizationName {get;set;}

    // Get variable which is being called by vf component.
    public Id RecipientId
    {
        get
        {
            RecipientId = PersonId;
            GetMissingEntityForRound3();
            return RecipientId;
        } set;
    }

    // Method to get 'X12_31_Round_3_Missing_Documents__c' from the deal.
    public ENT_CTRL_LIHTC_Late_Notification_R3()
    {
        MapOfDealWithMissingEntitiesRound3 = new Map<string, List<String>>();
    }

    public Map<string, List<String>> GetMissingEntityForRound3()
    {
        Set<Id> setOfDealIds = new Set<Id>();

        for(ENT_External_Resource__c er: [SELECT Id, Person__r.Contact__c,Account__r.Name, LIHTC_Deal__r.Deal_ID__c, LIHTC_Deal__r.Partnership__r.Name, LIHTC_Deal__r.X12_31_Round_3_Missing_Documents__c, LIHTC_Deal__r.Primary_Accounting_Manager__c FROM ENT_External_Resource__c WHERE Contact_Role__c = 'Sponsor Contact' AND primary__c = true AND Active__c = true AND
                                        ((LIHTC_Deal__r.Project_Status__c = 'Closed Pmt Disbursed' AND Actual_Closing_Date__c != THIS_YEAR) OR (LIHTC_Deal__r.Project_Status__c = 'Sold' AND LIHTC_Deal__r.Sold_Date_Actual__c != LAST_YEAR)) AND (LIHTC_Deal__r.FundStr__c != null) AND
                                        (NOT LIHTC_Deal__r.FundStr__c like '%NYEF%') AND (NOT LIHTC_Deal__r.FundStr__c like '%EMOF%') AND (NOT LIHTC_Deal__r.FundStr__c like '%481%') AND (NOT LIHTC_Deal__r.FundStr__c like '%TDBUSA%') AND
                                        (LIHTC_Deal__r.Asset_Manager__r.Name != 'Monica Spillane') and LIHTC_Deal__r.X12_31_Round_3_Missing_Documents__c !='' and Person__r.Contact__c =:PersonId limit 1])
        {
            setOfDealIds.add(er.LIHTC_Deal__c);
            if(OrganizationName == null)
            {
                OrganizationName = er.Account__r.Name;
            }
            if(!MapOfDealWithMissingEntitiesRound3.containsKey(er.LIHTC_Deal__r.Deal_ID__c + '#' + er.LIHTC_Deal__r.Partnership__r.Name))
            {
                String strTemp = er.LIHTC_Deal__r.X12_31_Round_3_Missing_Documents__c;
                List<String> lstDocs = strTemp.split('#');
                MapOfDealWithMissingEntitiesRound3.put(er.LIHTC_Deal__r.Deal_ID__c + '#' + er.LIHTC_Deal__r.Partnership__r.Name, lstDocs);
            }
        }

        for(ENT_Enterprise_Team__c team: [Select Id, User__c, User__r.Name, User__r.Email From ENT_Enterprise_Team__c where Role__c = 'Accounting Manager' and Primary__c = true and Active__c = true and LIHTC_Deal__c in :SetofDealIds limit 1])
        {
            if(team.User__c != null)
            {
                AccountingManager = team.User__r.Name;
                AccountingManagerMailId = team.User__r.Email;
            }
        }

        return MapOfDealWithMissingEntitiesRound3;
    }
}