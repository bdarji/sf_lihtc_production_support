/**************************************************************
Class   : ENT_Batch_LIHTC_Late_Notify_SetFlag_R2
Author  : Bdarji
Date    : 3/12/2020
Details : Related ticket #EHCI-376
          - This batch will identidy deals and set a (X12_31_Round_2_Document_Missing__c) with miss document names which falls under missing document criteria.
          - criteria is mentioned on the ticket EHCI-376.
          - for such deal's this batch will set a missing doc list field, then that is used in query mentioned in reminder record.
Test Class : TEST_TestCoverage_EPIC15 (83%)
History : v1.0 - 3/12/2020 - Bdarji -  Created
          v1.1 - 4/14/2020 - Bdarji - 1. Added fiter to incorporate sold deals also along with 'CPD' deals.
                                      2. Added filter to 'Actual_Closing_Date__c' to exclude deals if closed in current year.(2020)
                                      3. Added filter to 'Sold_Date_Actual__c' to exclude deals if sold in last year/current reporting year(2019), however filter was already added on Line no. 181.
                                      4. TO REUSE SAME BATCH(since only recipients and due date are different)For Round-3 Reminder, added logic to identify current reminder(Round-2 OR Round-3) and
                                         assign value in according field.(X12_31_Round_2_Missing_Documents__c OR X12_31_Round_3_Missing_Documents__c)
                                      5. As per Paula's comment(EHCI-348), modified 'Sold_Date_Actual__c' filter, to remove deal even if its sold before 'current reporting year' (2019).
*****************************************************************/
global class ENT_Batch_LIHTC_Late_Notify_SetFlag_R2 implements Schedulable, Database.Batchable<sObject>, Database.Stateful
{
    // Query to get deal records
    global string StrQuery;

    // v1.1 starts
    global String PickedReminder;
    // v1.1 ends

    // list of final save result to gether all details passed/failed records and send all detail in the mail to batch owner.
    private List<Database.SaveResult> lstDbSaveResult_Final = new List<Database.SaveResult>();

    // Hold final result string to append in message body.
    private String final_Result_String;
    private String exception_String;

    // constructor
    global ENT_Batch_LIHTC_Late_Notify_SetFlag_R2()
    {
        // In Below query we have added fixed set of parameters, which are specific to the/this late notification,
        // Below parameters here are acts like simple filter just to avoide unnecessary results/records.
        // v1.1 Starts (added sold date filter, Round-3 field)
        StrQuery = 'Select Id, Name, X12_31_Round_2_Missing_Documents__c, X12_31_Round_3_Missing_Documents__c, (SELECT id, LIHTC_Deal__c from External_Resources1__r where Active__c = true and Primary__c = true and Role__c = \'CPA Firm\' and Resource_Name__c like \'%CohnReznick%\') From ENT_ED_LIHTC__c Where ((Project_Status__c = \'Closed Pmt Disbursed\' AND Actual_Closing_Date__c != THIS_YEAR) OR (Project_Status__c = \'Sold\' AND (Sold_Date_Actual__c > LAST_YEAR or Sold_Date_Actual__c = null))) AND (FundStr__c != null) AND (NOT FundStr__c like \'%NYEF%\') AND (NOT FundStr__c like \'%EMOF%\') AND (NOT FundStr__c like \'%481%\') AND (NOT FundStr__c like \'%TDBUSA%\') AND (Asset_Manager__r.Name != \'Monica Spillane\')';
        // v1.1 Ends
    }

    // Below method will get deal records using above query.
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(StrQuery);
    }

    // Below method will pick 200 deal records at once and enter name of missing document in 'X12_31_Round_2_Missing_Documents__c' field(of deal).
    global void execute(Database.BatchableContext BC, List<ENT_ED_LIHTC__c> scope)
    {
        Set<Id> setofDealIds = new Set<Id>();
        Set<Id> setOfCohnReznickDealIds = new Set<Id>();

        for(ENT_ED_LIHTC__c deal: scope)
        {
            setofDealIds.add(deal.Id);
            for(ENT_External_Resource__c er :deal.External_Resources1__r)
            {
                setOfCohnReznickDealIds.add(er.LIHTC_Deal__c);
            }
        }

        // START : CORE LOGIC TO IDENTIFY PRE-REQUISITS STARTS

        // set to hold id's of entities and its parent to be used later in main query, where we actually identifying missing documents.
        set<Id> setOfFinancialReceivedPrntIdToExclude = new set<Id>();
        set<Id> setOfDraftReceivedPrntIdToExclude = new set<Id>();
        set<Id> setOfFinancialAntiDatePrntIdToExclude = new set<Id>();
        set<Id> setOfDraftAntiDatePrntIdToExclude = new set<Id>();
        set<Id> setOfFinancialCMiniPrntIdToExclude = new set<Id>();
        Date DateOfFifteenthFeb = Date.newInstance(System.Today().year(), 2, 15);
        Date DateOfFirstMarch = Date.newInstance(System.Today().year(), 3, 1);
        set<Id> setOfDrftFinDeadPrntId = new set<Id>();
        set<Id> setOfDrftTaxRetDeadPrntId = new set<Id>();
        set<Id> setOfFinlFinDeadPrntId = new set<Id>();
        set<Id> setOfFinlTRetDeadPrntId = new set<Id>();
        set<Id> etOfFinalReqMissingPrntId = new set<Id>();
        set<Id> setOfDraftReqMissingId = new set<Id>();
        set<Id> setOfDraftFSReqRcvdId = new set<Id>();
        set<Id> setOfDraftFSReqRcvdPrntId = new set<Id>();
        set<Id> setOfDraftTRReqRcvdId = new set<Id>();
        set<Id> setOfDraftTRReqRcvdPrntId = new set<Id>();
        set<Id> setOfDraftFinclReqMissingPrntId = new set<Id>();
        set<Id> setOfDraftTaxRetReqMissingPrntId = new set<Id>();

        for(ENT_Inbound_Correspondence_Entity__c entity: [select Id, Name, Correspondence_Type__c, Date_Received__c, Inbound_Correspondence__c, Anticipated_Received_Date__c,
                                                          Inbound_Correspondence__r.Review_Rating__c, Inbound_Correspondence__r.LIHTC_Deal__c, Deadline_Date__c, Not_Required__c
                                                          From ENT_Inbound_Correspondence_Entity__c Where Reporting_Year__c =: String.valueOf(System.Today().year() - 1) and
                                                          Inbound_Correspondence__r.LIHTC_Deal__c in :setofDealIds and
                                                          (Correspondence_Type__c = 'Draft Financial Statement' or Correspondence_Type__c = 'Final Financial Statement' or
                                                          Correspondence_Type__c = 'Draft Tax Return' or Correspondence_Type__c = 'Final Tax Return')])
        {
            // CRITERIA - 1 : If there is a date in the draft or final financial statement – remove the financial statement
            if((entity.Correspondence_Type__c == 'Draft Financial Statement' || entity.Correspondence_Type__c == 'Final Financial Statement') && entity.Date_Received__c != null)
            {
                setOfFinancialReceivedPrntIdToExclude.add(entity.Inbound_Correspondence__c);
            }

            // CRITERIA - 2 : If there is a date in the draft or final tax return – remove tax return
            if((entity.Correspondence_Type__c == 'Draft Tax Return' || entity.Correspondence_Type__c == 'Final Tax Return') && entity.Date_Received__c != null)
            {
                setOfDraftReceivedPrntIdToExclude.add(entity.Inbound_Correspondence__c);
            }

            // CRITERIA - 3 : If there is an Anticipated Receipt Date and that date is today or greater for the draft else final financial statement – remove financial statement
            if((entity.Correspondence_Type__c == 'Draft Financial Statement' || entity.Correspondence_Type__c == 'Final Financial Statement') && entity.Anticipated_Received_Date__c >= system.today())
            {
                setOfFinancialAntiDatePrntIdToExclude.add(entity.Inbound_Correspondence__c);
            }

            // CRITERIA - 4 : If there is an Anticipated Receipt Date and that date is today or greater for the draft else final tax return – remove tax return
            if((entity.Correspondence_Type__c == 'Draft Tax Return' || entity.Correspondence_Type__c == 'Final Tax Return') && entity.Anticipated_Received_Date__c >= system.today())
            {
                setOfDraftAntiDatePrntIdToExclude.add(entity.Inbound_Correspondence__c);
            }

            // CRITERIA - 5 : If review rating equals C/Mini, and the draft else final financial statement is blank, and the CPA Firm contains CohnReznick, remove Financial Statement
            // BLANK here means Date_Received__c == null
            if((entity.Correspondence_Type__c == 'Draft Financial Statement' || entity.Correspondence_Type__c == 'Final Financial Statement') && entity.Inbound_Correspondence__r.Review_Rating__c == 'C/Mini' && entity.Date_Received__c == null && setOfCohnReznickDealIds.contains(entity.Inbound_Correspondence__r.LIHTC_Deal__c))
            {
                setOfFinancialCMiniPrntIdToExclude.add(entity.Inbound_Correspondence__c);
            }

            // CRITERIA - 6 :
            // If there is Deadline Date for  - draft financial statement greater than 2/15 and final financial statement greater than- 3/1 - Remove financial statement
            // If there is Deadline Date for  - draft tax return greater than 2/15 and final tax return greater than - 3/1 - Remove tax return
            if((entity.Correspondence_Type__c == 'Draft Financial Statement' || entity.Correspondence_Type__c == 'Draft Tax Return') && entity.Deadline_Date__c > DateOfFifteenthFeb)
            {
                if(entity.Correspondence_Type__c == 'Draft Financial Statement')
                {
                    setOfDrftFinDeadPrntId.add(entity.Inbound_Correspondence__c);
                }
                else
                {
                    setOfDrftTaxRetDeadPrntId.add(entity.Inbound_Correspondence__c);
                }
            }
            if((entity.Correspondence_Type__c == 'Final Financial Statement' || entity.Correspondence_Type__c == 'Final Tax Return') && entity.Deadline_Date__c > DateOfFirstMarch)
            {
                if(entity.Correspondence_Type__c == 'Final Financial Statement')
                {
                    setOfFinlFinDeadPrntId.add(entity.Inbound_Correspondence__c);
                }
                else
                {
                    setOfFinlTRetDeadPrntId.add(entity.Inbound_Correspondence__c);
                }
            }

            // CRITERIA - 7 : If draft is required and not received, ask for the draft in the reminder and not final - applicable for audit and tax returns both.
            if((entity.Correspondence_Type__c == 'Draft Financial Statement' || entity.Correspondence_Type__c == 'Draft Tax Return') && entity.Not_Required__c == false && entity.Date_Received__c == null)
            {
                setOfDraftReqMissingId.add(entity.Id);
                if(entity.Correspondence_Type__c == 'Draft Financial Statement')
                {
                    setOfDraftFinclReqMissingPrntId.add(entity.Inbound_Correspondence__c);
                }
                else
                {
                    setOfDraftTaxRetReqMissingPrntId.add(entity.Inbound_Correspondence__c);
                }
            }

            // CRITERIA - 8 : If draft is required and received, final should not be listed.
            if((entity.Correspondence_Type__c == 'Draft Financial Statement' || entity.Correspondence_Type__c == 'Draft Tax Return') && entity.Not_Required__c == false && entity.Date_Received__c != null)
            {
                if(entity.Correspondence_Type__c == 'Draft Financial Statement')
                {
                    setOfDraftFSReqRcvdId.add(entity.Id);
                    setOfDraftFSReqRcvdPrntId.add(entity.Inbound_Correspondence__c);
                }
                else
                {
                    setOfDraftTRReqRcvdId.add(entity.Id);
                    setOfDraftTRReqRcvdPrntId.add(entity.Inbound_Correspondence__c);
                }
            }
        }
        // END : CORE LOGIC TO IDENTIFY PRE-REQUISITS ENDS

        // QUERY TO FIND OUT MISSING ITEMS AS REQUIRED USING ABOVE FOUND- PREREQUISITS.
        Map<Id, String> mapOfDealWithMissingItemNames = new Map<Id, String>();
        // v1.1 Starts/Ends (removed sold date filter and field from below query, added it along with sold status filter in main query)
        for(ENT_Inbound_Correspondence_Entity__c entity: [Select Id, Name, Deadline_Date__c, Correspondence_Type__c, Inbound_Correspondence__c, Inbound_Correspondence__r.LIHTC_Deal__c,
                                                          Inbound_Correspondence__r.Review_Rating__c From ENT_Inbound_Correspondence_Entity__c Where Reporting_Year__c =: String.valueOf(System.Today().year() - 1)
                                                          and Inbound_Correspondence__r.LIHTC_Deal__c in :setofDealIds and
                                                          ((Correspondence_Type__c = 'Draft Financial Statement'
                                                          AND Inbound_Correspondence__c not in : setOfFinlFinDeadPrntId
                                                          and Inbound_Correspondence__c not in : setOfFinancialReceivedPrntIdToExclude and
                                                          Inbound_Correspondence__c not in : setOfFinancialAntiDatePrntIdToExclude and Inbound_Correspondence__c not in : setOfFinancialCMiniPrntIdToExclude and
                                                          Inbound_Correspondence__c not in : setOfDrftFinDeadPrntId and Id in :setOfDraftReqMissingId and Id not in :setOfDraftFSReqRcvdId) OR
                                                          (Correspondence_Type__c = 'Final Financial Statement'
                                                          AND Inbound_Correspondence__c not in : setOfDrftFinDeadPrntId
                                                          and Inbound_Correspondence__c not in : setOfFinancialReceivedPrntIdToExclude and
                                                          Inbound_Correspondence__c not in : setOfFinancialAntiDatePrntIdToExclude and Inbound_Correspondence__c not in : setOfFinancialCMiniPrntIdToExclude and
                                                          Inbound_Correspondence__c not in : setOfFinlFinDeadPrntId and Inbound_Correspondence__c not in : setOfDraftFinclReqMissingPrntId and
                                                          Inbound_Correspondence__c not in : setOfDraftFSReqRcvdPrntId) OR (Correspondence_Type__c = 'Draft Tax Return'
                                                          AND Inbound_Correspondence__c not in : setOfFinlTRetDeadPrntId
                                                          and Inbound_Correspondence__c not in :
                                                          setOfDraftReceivedPrntIdToExclude and Inbound_Correspondence__c not in : setOfDraftAntiDatePrntIdToExclude and Inbound_Correspondence__c not in :
                                                          setOfDrftTaxRetDeadPrntId and Id in: setOfDraftReqMissingId and Id not in : setOfDraftTRReqRcvdId) OR
                                                          (Correspondence_Type__c = 'Final Tax Return'
                                                          AND Inbound_Correspondence__c not in : setOfDrftTaxRetDeadPrntId
                                                          and Inbound_Correspondence__c not in : setOfDraftReceivedPrntIdToExclude and
                                                          Inbound_Correspondence__c not in : setOfDraftAntiDatePrntIdToExclude and Inbound_Correspondence__c not in : setOfFinlTRetDeadPrntId and
                                                          Inbound_Correspondence__c not in : setOfDraftTaxRetReqMissingPrntId and Inbound_Correspondence__c not in : setOfDraftTRReqRcvdPrntId))])
        {
            if(!mapOfDealWithMissingItemNames.containsKey(entity.Inbound_Correspondence__r.LIHTC_Deal__c))
            {
                mapOfDealWithMissingItemNames.put(entity.Inbound_Correspondence__r.LIHTC_Deal__c, entity.Inbound_Correspondence__r.Review_Rating__c == 'C/Mini' && entity.Correspondence_Type__c == 'Draft Financial Statement' ? 'Mini Audit Documents' : entity.Correspondence_Type__c);
            }
            else
            {
                String strDoc = mapOfDealWithMissingItemNames.get(entity.Inbound_Correspondence__r.LIHTC_Deal__c);
                String strUpdatedDocList = strDoc + '#' + entity.Correspondence_Type__c;
                mapOfDealWithMissingItemNames.put(entity.Inbound_Correspondence__r.LIHTC_Deal__c, strUpdatedDocList);
            }
        }

        // list to hold deals which are going to update.
        List<ENT_ED_LIHTC__c> lstDealsToUpdate = new List<ENT_ED_LIHTC__c>();

        // v1.1 Starts (logic to identidy current reminder)
        /*
            - WE HAVE RETRIEVED QUERY OF MAIN BATCH(IJPARK), WHICH SENDS AN EMAILS TO RECIPIENTS LISTED UNDER ANY COMMUNITY REMINDER RECORD.
            - THE REASON BEHIND THIS IS AT ANY POINT OF TIME, WE WANTED TO GET THE SAME LIST OF REMINDERS, WHICH THAT BATCH WILL PICK AT THAT TIME.
            - WE HAVE PULLED VARIABLE VALUES ALSO FROM THERE, SO IF ANY CHANGE(IN QUERY OR VARIABLE) WILL HAPPENED THERE, WILL AUTOMATICALLY REFLECT HERE.
        */
        ENT_Batch_Community_Email_R_Reminder btchToPickReminder = new ENT_Batch_Community_Email_R_Reminder();
        Date minDueDate = btchToPickReminder.minDueDate;
        Date today = btchToPickReminder.today;
        String strQryToPickReminders = btchToPickReminder.commArtQuery;

        for(ENT_Community_Reminder__c cr : Database.query(strQryToPickReminders))
        {
            if(((cr.Title__c.contains('Round 2') && cr.Title__c.contains('FRC')) || (Test.isRunningTest() && cr.Title__c.contains('TestCase') && cr.Email_Template__c.contains('FRC'))) && cr.Notification_Type__c == 'One Time' && cr.Type__c == 'SOQL' && cr.Related_Community_Application__c == 'Custom')
            {
                PickedReminder = 'Round_2';
                break;
            }
            else if(((cr.Title__c.contains('Round 3') && cr.Title__c.contains('Sponsor')) || (Test.isRunningTest() && cr.Title__c.contains('TestCase') && cr.Email_Template__c.contains('Sponsor'))) && cr.Notification_Type__c == 'One Time' && cr.Type__c == 'SOQL' && cr.Related_Community_Application__c == 'Custom')
            {
                PickedReminder = 'Round_3';
                break;
            }
        }
        // v1.1 Ends

        // v1.1 Start (Added check to assign value according to picked reminder only when remidner is identified)
        if(!String.isEmpty(PickedReminder))
        {
            // ITERATE OVER THE SCOPE/ALL DEALS AND ASSIGN VALUE IN TEXT FIELD.
            for(ENT_ED_LIHTC__c deal: scope)
            {
                if(mapOfDealWithMissingItemNames.containsKey(deal.Id))
                {
                    // update missing document field if picked up reminder is 'Round_2'(FRC/EHCI-376)
                    if(PickedReminder == 'Round_2')
                    {
                        if(deal.X12_31_Round_2_Missing_Documents__c != mapOfDealWithMissingItemNames.get(deal.Id))
                        {
                            deal.X12_31_Round_2_Missing_Documents__c = mapOfDealWithMissingItemNames.get(deal.Id);
                            lstDealsToUpdate.add(deal);
                        }
                    }
                    // update missing document field if picked up reminder is 'Round_3'(sponsor/EHCI-348)
                    else if(PickedReminder == 'Round_3')
                    {
                        if(deal.X12_31_Round_3_Missing_Documents__c != mapOfDealWithMissingItemNames.get(deal.Id))
                        {
                            deal.X12_31_Round_3_Missing_Documents__c = mapOfDealWithMissingItemNames.get(deal.Id);
                            lstDealsToUpdate.add(deal);
                        }
                    }
                }
                else
                {
                    // update missing document field if picked up reminder is 'Round_2'(FRC/EHCI-376)
                    if(PickedReminder == 'Round_2')
                    {
                        if(deal.X12_31_Round_2_Missing_Documents__c != '' && deal.X12_31_Round_2_Missing_Documents__c != null)
                        {
                            deal.X12_31_Round_2_Missing_Documents__c = '';
                            lstDealsToUpdate.add(deal);
                        }
                    }
                    // update missing document field if picked up reminder is 'Round_3'(sponsor/EHCI-348)
                    else if(PickedReminder == 'Round_3')
                    {
                        if(deal.X12_31_Round_3_Missing_Documents__c != '' && deal.X12_31_Round_3_Missing_Documents__c != null)
                        {
                            deal.X12_31_Round_3_Missing_Documents__c = '';
                            lstDealsToUpdate.add(deal);
                        }
                    }
                }
            }
        }
        // v1.1 End (Added check to assign value according to picked reminder only when remidner is identified)

        // CHECK LIST IS NOT EMPTY BEFORE DML
        if(!lstDealsToUpdate.isEmpty())
        {
            try
            {
                List<Database.SaveResult> lstDBSaveRsltDeals = Database.update(lstDealsToUpdate, false);
                lstDbSaveResult_Final.addAll(lstDBSaveRsltDeals);
            }
            catch (exception ex)
            {
                exception_String += ex.getStackTraceString() + '\n';
            }
        }
    }

    // METHOD WILL BE EXECUTED ONCE BATCH IS FINISHED EXECUTION.
    global void finish(Database.BatchableContext BC)
    {
        long NoOfUpdatedRecords = 0;

        // Iterate over all results and generate 'final_Result_String' to send email to notify user about failed records.
        for(Database.SaveResult singleresult : lstDbSaveResult_Final)
        {
            // Check to make sure iterating over only unsuccess/failed results.
            if(singleresult.isSuccess() == false)
            {
                if(String.isBlank(final_Result_String))
                {
                    final_Result_String = 'Please find detail of record(s) which failed during update' + '\n\n' + 'Record Id : ' + singleresult.getId() + ' Error : ' + singleresult.getErrors() + '\n';
                }
                else
                {
                    final_Result_String += 'Record Id : ' + singleresult.getId() + ' Error : ' + singleresult.getErrors() + '\n';
                }
            }
            else
            {
                NoOfUpdatedRecords += 1;
                // Send single line in mail stating successful execution of batch.
                final_Result_String = NoOfUpdatedRecords + ' LIHTC Deal record(s) successfully updated for ' + PickedReminder + '  Late Notice.';
            }
        }

        // Define mail body and recipients.
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] {Label.ENT_Batch_LIHTC_Late_Notify_SetFlag_Owner};
        message.subject = 'Batch execution result of : ' + date.today().format() + ' : ENT_Batch_LIHTC_Late_Notify_SetFlag_R2 For ' + PickedReminder;
        String Str_exception_String = exception_String != null ? exception_String : '';
        message.plainTextBody = final_Result_String != null ? final_Result_String : 'No Records to update'   +  Str_exception_String ;
        message.setHtmlBody(final_Result_String != null ? final_Result_String : 'No Records to update'   +   Str_exception_String);
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};

        // Track Email Sent Result
        try
        {
            if(Label.ENT_Batch_LIHTC_Late_Notify_SetFlag_Owner != null)
            {
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

                if (results[0].success)
                {
                    System.debug('The email was sent successfully.');
                }
                else
                {
                    System.debug('The email failed to send: ' + results[0].errors[0].message);
                }
            }
        }
        catch (Exception ex)
        {
            System.debug('The following exception has occurred: ' + ex.getMessage());
        }
    }

    // below method will schedule above batch with given cron expression and will be executed as a part of schedulable interface.
    global void execute(SchedulableContext sc)
    {
        Database.executeBatch(new ENT_Batch_LIHTC_Late_Notify_SetFlag_R2());
    }
}