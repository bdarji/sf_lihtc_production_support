/***************************************************************
Class   : Test_ReviewChecklist_Triggers
Author  : NBanas
Date    : 05/17/2013
Details : Tests both the ENT_Inbound_Correspondence_Entity_Create_CL
          and ENT_Review_Checklist_Trigger triggers.
History : v1.0 - 05/17/2013 - Created 
          v1.1 - 01/08/2020 - Added new method(TestCase4).
          v1.2 - 5/18/2020 - bdarji - re-positioned 'test.start/stop', to reset 'SOQL' limits in 2 test methods.
*****************************************************************/

public class Test_ReviewChecklist_Triggers
{
    @isTest(SeeAllData = True)
    public static void TestCase1()
    {
        ENT_Project__c testProj = new ENT_Project__c(Name='Test Proj ZZZ');
        insert testProj;
        
        RecordType rt = [select id, name, sObjectType from RecordType where Name = 'NYEF Deal' and sObjectType ='ENT_ED_LIHTC__c' limit 1];
        RecordType AMFART = [select Id, Name, sObjectType from RecordType where sObjectType = 'ENT_Review_Checklist__c' and Name = 'Asset Management' limit 1];
        
        ENT_ED_LIHTC__c testDeal = new ENT_ED_LIHTC__c(Name = 'Test Deal ZZZ', Project__c = testProj.Id, RecordTypeId = rt.Id, Project_Status__c = 'Closed Pmt Disbursed');
        insert testDeal;
        
        ENT_Review_Checklist__c relatedChecklist = null;
        User user2 = [SELECT Id FROM User WHERE Firstname='Brijesh' AND isActive=true limit 1];
        
        ENT_Inbound_Correspondence__c testIBC = [select id, name, Review_Rating__c, LIHTC_Deal__c, 
            (select Id, Name, Correspondence_Type__c, Status__c, Date_Received__c from Inbound_Correspondence_Entities__r order by Name),
            (select Id, Name, Type__c, Related_Entity_Type__c, Checklist_Type__c, Status__c from Review_Checklists__r order by Name)
            from ENT_Inbound_Correspondence__c
            where LIHTC_Deal__c = :testDeal.Id limit 1];
        
        ENT_Inbound_Correspondence__c testIBC1 = new ENT_Inbound_Correspondence__c(LIHTC_Deal__c = testDeal.id, Name = '2015');
        insert testIBC1;
        ENT_Inbound_Correspondence_Entity__c inboundCorresEnt1 = new ENT_Inbound_Correspondence_Entity__c(Additional_Attribute__c = null, Reviewer__c=user2.Id, Correspondence_Type__c='Draft Tax Return', Inbound_Correspondence__c = testIBC1.Id, Status__c='Data Entry Complete');
        insert inboundCorresEnt1;
        
        ENT_Inbound_Correspondence__c testIBC2 = new ENT_Inbound_Correspondence__c(LIHTC_Deal__c = testDeal.id, Name = '2016');
        insert testIBC2;
        ENT_Inbound_Correspondence_Entity__c inboundCorresEnt2 = new ENT_Inbound_Correspondence_Entity__c(Additional_Attribute__c = 'Sold Tech Term',Reviewer__c=user2.Id,  Correspondence_Type__c='Final Tax Return', Inbound_Correspondence__c = testIBC2.Id, Status__c='Data Entry Complete');
        insert inboundCorresEnt2;
        
        List<ENT_Review_Checklist__c> checklistsForInsert4 = new List<ENT_Review_Checklist__c>();
        checklistsForInsert4.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 3', RecordTypeId = AMFART.Id, Type__c = 'Quarterly', Uses_Approval_Process__c = true, Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Parent_Checklist__c = null, Period__c = 'Quarter 3', Related_Entity_Type__c = 'Financial Statement', Reporting_Checklist__c = testIBC.Id));
        checklistsForInsert4.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 4', RecordTypeId = AMFART.Id, Type__c = 'Quarterly',Uses_Approval_Process__c = true, Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Parent_Checklist__c = null, Period__c = 'Quarter 3', Related_Entity_Type__c = 'AMFA', Reporting_Checklist__c = testIBC.Id));
  
        
        testIBC.Review_Rating__c = 'B';
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        insert checklistsForInsert4;
        update testIBC;
        
        
        Map<id, ENT_Inbound_Correspondence_Entity__c> IBCForUpdate = new Map<id, ENT_Inbound_Correspondence_Entity__c>();
        
        id qFS = null;
        id aFS = null;
        id aTR = null;
        
        for(ENT_Inbound_Correspondence_Entity__c e : testIBC.Inbound_Correspondence_Entities__r)
        {
            e.Status__c = '';
            relatedChecklist = checklistsForInsert4[0];
            if(e.Correspondence_Type__c.contains('Financial Statement') && e.Correspondence_Type__c.contains('Quarter'))
            {
                e.Status__c = 'No DQ Issues';
                e.Date_Received__c = system.today();
                //e.Additional_Attribute__c = 'Mini Audit';
                e.Additional_Attribute__c = 'Sold Tech Term';             
                qFS = qFS == null ? e.Id : qFS;
            }
            else if(e.Correspondence_Type__c.contains('Financial Statement') && !e.Correspondence_Type__c.contains('Quarter'))
            {
                e.Status__c = 'Data Entry Complete';
                e.Date_Received__c = system.today();
                e.Additional_Attribute__c = 'Sold Tech Term';                
                aFS = aFS == null ? e.Id : aFS;
            }
            else if(e.Correspondence_Type__c.contains('Tax Return') && !e.Correspondence_Type__c.contains('Amended'))
            {
                e.Additional_Attribute__c = 'Sold Tech Term';
                e.Status__c = 'Data Entry Complete';
                e.Date_Received__c = system.today();
                aTR = aTR == null ? e.Id : aTR;
            }
            else if(e.Correspondence_Type__c.contains('Final') && e.Date_Received__c != null && checklistsForInsert4[0].Type__c == 'Draft')
            {
                e.Status__c = '';
                e.Date_Received__c = system.today();
                e.Additional_Attribute__c = 'Sold Tech Term';
                aTR = aTR == null ? e.Id : aTR;
            }
            
            if(e.Status__c != '')
            {
                IBCForUpdate.put(e.Id, e);
            }
        }
        
        test.startTest();
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        update IBCForUpdate.values();
        
        // Check for Status Change
        if(qFS != null)
            IBCForUpdate.get(qFS).Status__c = 'In Supervisory Review';
        if(aFS != null)
            IBCForUpdate.get(aFS).Status__c = 'In Supervisory Review';
        if(aTR != null)
            IBCForUpdate.get(aTR).Status__c = 'In Supervisory Review';
        
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        update IBCForUpdate.values();
        
        test.stopTest();
    }
    
    @isTest(SeeAllData = True)
    public static void TestCase2()
    {
        ENT_Project__c testProj = new ENT_Project__c(Name='Test Proj ZZZ');
        insert testProj;
        
        RecordType rt = [select id, name, sObjectType from RecordType where Name = 'Standard Deal' and sObjectType ='ENT_ED_LIHTC__c' limit 1];
        
        List<ENT_Review_Checklist__c> checklistsForInsert5 = new List<ENT_Review_Checklist__c>();
        ENT_ED_LIHTC__c testDeal = new ENT_ED_LIHTC__c(Name = 'Test Deal ZZZ', Project__c = testProj.Id, RecordTypeId = rt.Id, Project_Status__c = 'Closed Pmt Disbursed');
        insert testDeal;
        

        
        ENT_Inbound_Correspondence__c testIBC = [select id, name, Review_Rating__c, LIHTC_Deal__c, 
            (select Id, Name, Correspondence_Type__c, Status__c, Date_Received__c from Inbound_Correspondence_Entities__r order by Name),
            (select Id, Name, Type__c, Related_Entity_Type__c, Checklist_Type__c, Status__c from Review_Checklists__r order by Name)
            from ENT_Inbound_Correspondence__c
            where LIHTC_Deal__c = :testDeal.Id limit 1];
        
        testIBC.Review_Rating__c = 'B';
        
        update testIBC;
        
        Map<id, ENT_Inbound_Correspondence_Entity__c> IBCForUpdate = new Map<id, ENT_Inbound_Correspondence_Entity__c>();
        
        id qFS = null;
        id daFS = null;
        id faFS = null;
        id daTR = null;
        id faTR = null;
        
        for(ENT_Inbound_Correspondence_Entity__c e : testIBC.Inbound_Correspondence_Entities__r)
        {
            e.Status__c = '';
        
            if(e.Correspondence_Type__c.contains('Financial Statement') && e.Correspondence_Type__c.contains('Quarter'))
            {
                e.Status__c = 'No DQ Issues';
                e.Date_Received__c = system.today();
                //e.Additional_Attribute__c = 'Mini Audit';
                qFS = qFS == null ? e.Id : qFS;
            }
            else if(e.Correspondence_Type__c.contains('Financial Statement') && !e.Correspondence_Type__c.contains('Quarter'))
            {
                e.Status__c = 'Data Entry Complete';
                e.Date_Received__c = system.today();
                e.Additional_Attribute__c = 'Mini Audit';
                if(e.Correspondence_Type__c.contains('Draft'))
                    daFS = daFS == null ? e.Id : daFS;
                else if(e.Correspondence_Type__c.contains('Final'))
                    faFS = faFS == null ? e.Id : faFS;
            }
            else if(e.Correspondence_Type__c.contains('Tax Return') && !e.Correspondence_Type__c.contains('Amended'))
            {
                e.Status__c = 'Data Entry Complete';
                e.Date_Received__c = system.today();
                
                if(e.Correspondence_Type__c.contains('Draft'))
                    daTR = daTR == null ? e.Id : daTR;
                else if(e.Correspondence_Type__c.contains('Final'))
                    faTR = faTR == null ? e.Id : faTR;
            }
            
           
            
            if(e.Status__c != '')
            {
                IBCForUpdate.put(e.Id, e);
            }
        }
        
       /* for(ENT_Inbound_Correspondence_Entity__c ent : testIBC.Inbound_Correspondence_Entities__r)
            {
            e.Status__c = '';
            if(e.Correspondence_Type__c.contains('Financial Statement') && ent.Correspondence_Type__c.contains('Quarter'))
            {
                e.Status__c = 'No DQ Issues';
                e.Date_Received__c = system.today();
                e.Additional_Attribute__c = 'Mini Audit';
                qFS = qFS == null ? ent.Id : qFS;
            }
            else if(e.Correspondence_Type__c.contains('Financial Statement') && !ent.Correspondence_Type__c.contains('Quarter'))
            {
                e.Status__c = 'Data Entry Complete';
                e.Date_Received__c = system.today();
                e.Additional_Attribute__c = 'Mini Audit';
                if(e.Correspondence_Type__c.contains('Draft'))
                    daFS = daFS == null ? e.Id : daFS;
                else if(e.Correspondence_Type__c.contains('Final'))
                    faFS = faFS == null ? e.Id : faFS;
            }
            else if(e.Correspondence_Type__c.contains('Tax Return') && !ent.Correspondence_Type__c.contains('Amended'))
            {
                e.Status__c = 'Data Entry Complete';
                e.Date_Received__c = system.today();
                e.Additional_Attribute__c = 'Mini Audit';
                if(e.Correspondence_Type__c.contains('Draft'))
                    daTR = daTR == null ? ent.Id : daTR;
                else if(e.Correspondence_Type__c.contains('Final'))
                    faTR = faTR == null ? ent.Id : faTR;
            }
            
            if(e.Status__c != '')
            {
                IBCForUpdate.put(ent.Id, e);
            }
        
        ENT_Review_Checklist__c relatedChecklist = null;*/

        RecordType rclRT = [select id, name, sObjectType from RecordType where Name = 'Tax and Regional Accounting' and sObjectType ='ENT_Review_Checklist__c' limit 1];
        RecordType AMFART = [select Id, Name, sObjectType from RecordType where sObjectType = 'ENT_Review_Checklist__c' and Name = 'Asset Management' limit 1];
        
        List<ENT_Review_Checklist__c> checklistsForInsert = new List<ENT_Review_Checklist__c>();
        List<ENT_Review_Checklist__c> checklistsForInsert2 = new List<ENT_Review_Checklist__c>();
        List<ENT_Review_Checklist__c> checklistsForInsert3 = new List<ENT_Review_Checklist__c>();
        
        checklistsForInsert.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 1', RecordTypeId = rclRT.Id, Type__c = 'Draft', Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Related_Entity_Type__c = 'Tax Return', Reporting_Checklist__c = testIBC.Id, Reporting_Checklist_Entity_Draft__c = daTR, Reporting_Checklist_Entity_Final__c = faTR));
        checklistsForInsert.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 2', RecordTypeId = rclRT.Id, Type__c = 'Draft', Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Related_Entity_Type__c = 'Financial Statement', Reporting_Checklist__c = testIBC.Id, Reporting_Checklist_Entity_Draft__c = daFS, Reporting_Checklist_Entity_Final__c = faFS));

        checklistsForInsert2.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 3', RecordTypeId = rclRT.Id, Type__c = 'Draft', Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Period__c = 'Quarter 2', Related_Entity_Type__c = 'Financial Statement', Reporting_Checklist__c = testIBC.Id, Reporting_Checklist_Entity_Draft__c = daFS, Reporting_Checklist_Entity_Final__c = faFS));
        checklistsForInsert2.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 4', RecordTypeId = rclRT.Id, Type__c = 'Draft', Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Period__c = 'Quarter 2', Related_Entity_Type__c = 'AMFA', Reporting_Checklist__c = testIBC.Id, Reporting_Checklist_Entity_Draft__c = daFS, Reporting_Checklist_Entity_Final__c = faFS));
        
        checklistsForInsert3.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 3', RecordTypeId = AMFART.Id, Type__c = 'Annual', Uses_Approval_Process__c = true, Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Parent_Checklist__c = null, Period__c = 'Quarter 3', Related_Entity_Type__c = 'AMFA', Reporting_Checklist__c = testIBC.Id, Reporting_Checklist_Entity_Draft__c = daFS, Reporting_Checklist_Entity_Final__c = faFS));
        checklistsForInsert3.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 4', RecordTypeId = AMFART.Id, Type__c = 'Quarterly',Uses_Approval_Process__c = true, Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Parent_Checklist__c = null, Period__c = 'Quarter 3', Related_Entity_Type__c = 'Tax Return', Reporting_Checklist__c = testIBC.Id, Reporting_Checklist_Entity_Draft__c = daFS, Reporting_Checklist_Entity_Final__c = faFS));
        
        
         
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        insert checklistsForInsert;
        
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = true;
        Utils.HasRun_ENT_Review_Checklist_Trigger = true;
        insert checklistsForInsert2;
        
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = true;
        Utils.HasRun_ENT_Review_Checklist_Trigger = true;
        insert checklistsForInsert3;
      
        User user1 = [SELECT Id FROM User WHERE Firstname='Brijesh' AND isActive=true limit 1];

        // Create an approval request for the account
        Approval.ProcessSubmitRequest req1 = 
            new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(checklistsForInsert3[0].id);

        // Submit on behalf of a specific submitter
        req1.setSubmitterId(user1.Id); 

        // Submit the record to specific process and skip the criteria evaluation
        req1.setProcessDefinitionNameOrId('RCL_QFS_As_Cs_Clone_2');
        req1.setSkipEntryCriteria(true);

        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);
        

        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        update IBCForUpdate.values();
        
        // Test Checklist Updates
        List<ENT_Review_Checklist__c> listRCL = [select Id, Name, Type__c, Related_Entity_Type__c, Checklist_Type__c, Status__c
            from ENT_Review_Checklist__c
            where Reporting_Checklist__c = :testIBC.Id];

        // v1.2 start/end - re-positioned below method to reset limits.(earlier was on line 250)
        test.startTest();
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        for(ENT_Review_Checklist__c rcl : listRCL)
        {
            rcl.Status__c = 'Pending Issues';
        }
        update listRCL;
        
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        for(ENT_Review_Checklist__c rcl : listRCL)
        {
            rcl.Status__c = 'In Supervisory Review';
        }
        update listRCL;
        
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        for(ENT_Review_Checklist__c rcl : listRCL)
        {
            rcl.Status__c = 'Approved As Is';
        }
        update listRCL;
        
        // Test AMFA Checklist Linking
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        update checklistsForInsert2;
        
        test.stopTest();
    }
    
    @isTest(SeeAllData = False)
    public static void TestCase3()
    {
        List<ENT_Inbound_Correspondence_Entity__c> lstInboundCorEntities=New List<ENT_Inbound_Correspondence_Entity__c>();
        ENT_Project__c testProj = new ENT_Project__c(Name='Test Proj');
        insert testProj;
        
        RecordType rt = [select id, name, sObjectType from RecordType where Name = 'NYEF Deal' and sObjectType ='ENT_ED_LIHTC__c' limit 1];
        RecordType AMFART = [select Id, Name, sObjectType from RecordType where sObjectType = 'ENT_Review_Checklist__c' and Name = 'Asset Management' limit 1];
        
        ENT_ED_LIHTC__c testDeal = new ENT_ED_LIHTC__c(Name = 'Test Deal', Project__c = testProj.Id, RecordTypeId = rt.Id, Project_Status__c = 'Closed Pmt Disbursed');
        insert testDeal;
        
        ENT_Review_Checklist__c relatedChecklist = null;
        User user2 = [SELECT Id FROM User WHERE Firstname='Brijesh' AND isActive=true limit 1];       
        //relatedChecklist.Draft_Reviewer__c = '0051400000BSztS';
        //relatedChecklist.Final_Reviewer__c = '0051400000BSztS';
        ENT_Inbound_Correspondence__c testIBC = new ENT_Inbound_Correspondence__c(LIHTC_Deal__c = testDeal.id, Name = '2015');
        insert testIBC;
        List<ENT_Review_Checklist__c> checklistsForInsert = new List<ENT_Review_Checklist__c>();
        checklistsForInsert.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 3', RecordTypeId = AMFART.Id, Type__c = 'Quarterly',
        Uses_Approval_Process__c = true,Draft_Reviewer__c = '0051400000BSztS', Status__c = 'No DQ Issues', Checklist_Type__c = 'B', Parent_Checklist__c = null, 
        Period__c = 'Quarter 3', Related_Entity_Type__c = 'Tax Return', Reporting_Checklist__c = testIBC.Id));     
        
        checklistsForInsert.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 4',Final_Reviewer__c = '0051400000BSztS', RecordTypeId = AMFART.Id, Type__c = 'Quarterly',
        Uses_Approval_Process__c = true, Status__c = 'No DQ Issues', Checklist_Type__c = 'B', Parent_Checklist__c = null, 
        Period__c = 'Quarter 3', Related_Entity_Type__c = 'Tax Return', Reporting_Checklist__c = testIBC.Id));
        
        checklistsForInsert.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 4',Final_Reviewer__c = '0051400000BSztS', RecordTypeId = AMFART.Id, Type__c = 'Annual',
        Uses_Approval_Process__c = true, Status__c = 'No DQ Issues', Checklist_Type__c = 'B', Parent_Checklist__c = null, 
        Period__c = 'Quarter 3', Related_Entity_Type__c = 'Financial Statement', Reporting_Checklist__c = testIBC.Id));
        
        checklistsForInsert.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 4',Final_Reviewer__c = '0051400000BSztS', RecordTypeId = AMFART.Id, Type__c = 'Quarterly',
        Uses_Approval_Process__c = true, Status__c = 'No DQ Issues', Checklist_Type__c = 'B', Parent_Checklist__c = null, 
        Period__c = 'Quarter 3', Related_Entity_Type__c = 'Financial Statement', Reporting_Checklist__c = testIBC.Id));
        
        testIBC.Review_Rating__c = 'B';
        update testIBC;
        insert checklistsForInsert;
        ENT_Inbound_Correspondence_Entity__c inboundCorresEnt1 = new ENT_Inbound_Correspondence_Entity__c(Additional_Attribute__c = 'Sold Tech Term', Correspondence_Type__c='Final Financial Statement', Inbound_Correspondence__c = testIBC.Id, Status__c='Data Entry Complete');
        
        ENT_Inbound_Correspondence_Entity__c inboundCorresEnt2 = new ENT_Inbound_Correspondence_Entity__c(Reviewer__c=user2.Id, Correspondence_Type__c='Final Financial Statement', Inbound_Correspondence__c = testIBC.Id, Status__c='Data Entry Complete');
        ENT_Inbound_Correspondence_Entity__c inboundCorresEnt3 = new ENT_Inbound_Correspondence_Entity__c(Additional_Attribute__c = 'Sold Tech Term',Reviewer__c=user2.Id, Correspondence_Type__c='Draft Tax Return', Inbound_Correspondence__c = testIBC.Id, Status__c='Data Entry Complete');
        ENT_Inbound_Correspondence_Entity__c inboundCorresEnt4 = new ENT_Inbound_Correspondence_Entity__c(Reviewer__c=user2.Id, Correspondence_Type__c='Quarter 3 Financial Statement', Inbound_Correspondence__c = testIBC.Id, Status__c='Pending Issues');
        ENT_Inbound_Correspondence_Entity__c inboundCorresEnt5 = new ENT_Inbound_Correspondence_Entity__c(Reviewer__c=user2.Id, Correspondence_Type__c='Final Financial Statement', Inbound_Correspondence__c = testIBC.Id, Status__c='Pending Issues');
        
        lstInboundCorEntities.add(inboundCorresEnt1);         
        lstInboundCorEntities.add(inboundCorresEnt2);
        lstInboundCorEntities.add(inboundCorresEnt3);
        lstInboundCorEntities.add(inboundCorresEnt4);
        lstInboundCorEntities.add(inboundCorresEnt5);
        
        test.startTest();
        insert lstInboundCorEntities;
        test.stopTest();
    }
    
    // v1.1 Starts
    @isTest(SeeAllData = True)
    public static void TestCase4()
    {
        ENT_Project__c testProj = new ENT_Project__c(Name='Test Proj ZZZ');
        insert testProj;
        
        RecordType rt = [select id, name, sObjectType from RecordType where Name = 'Standard Deal' and sObjectType ='ENT_ED_LIHTC__c' limit 1];
        
        List<ENT_Review_Checklist__c> checklistsForInsert5 = new List<ENT_Review_Checklist__c>();
        ENT_ED_LIHTC__c testDeal = new ENT_ED_LIHTC__c(Name = 'Test Deal ZZZ', Project__c = testProj.Id, RecordTypeId = rt.Id, Project_Status__c = 'Closed Pmt Disbursed');
        insert testDeal;
        
        ENT_Inbound_Correspondence__c testIBC = [select id, name, Review_Rating__c, LIHTC_Deal__c, 
            (select Id, Name, Correspondence_Type__c, Status__c, Date_Received__c from Inbound_Correspondence_Entities__r order by Name),
            (select Id, Name, Type__c, Related_Entity_Type__c, Checklist_Type__c, Status__c from Review_Checklists__r order by Name)
            from ENT_Inbound_Correspondence__c
            where LIHTC_Deal__c = :testDeal.Id limit 1];
        
        testIBC.Review_Rating__c = 'B';
        
        update testIBC;
        
        Map<id, ENT_Inbound_Correspondence_Entity__c> IBCForUpdate = new Map<id, ENT_Inbound_Correspondence_Entity__c>();
        
        id qFS = null;
        id daFS = null;
        id faFS = null;
        id daTR = null;
        id faTR = null;
        
        for(ENT_Inbound_Correspondence_Entity__c e : testIBC.Inbound_Correspondence_Entities__r)
        {
            e.Status__c = '';
        
            if(e.Correspondence_Type__c.contains('Financial Statement') && e.Correspondence_Type__c.contains('Quarter'))
            {
                e.Status__c = 'No DQ Issues';
                e.Date_Received__c = system.today();
                //e.Additional_Attribute__c = 'Mini Audit';
                qFS = qFS == null ? e.Id : qFS;
            }
            else if(e.Correspondence_Type__c.contains('Financial Statement') && !e.Correspondence_Type__c.contains('Quarter'))
            {
                e.Status__c = 'Data Entry Complete';
                e.Date_Received__c = system.today();
                e.Additional_Attribute__c = 'Mini Audit';
                if(e.Correspondence_Type__c.contains('Draft'))
                    daFS = daFS == null ? e.Id : daFS;
                else if(e.Correspondence_Type__c.contains('Final'))
                    faFS = faFS == null ? e.Id : faFS;
            }
            else if(e.Correspondence_Type__c.contains('Tax Return') && !e.Correspondence_Type__c.contains('Amended'))
            {
                e.Status__c = 'Data Entry Complete';
                e.Date_Received__c = system.today();
                
                if(e.Correspondence_Type__c.contains('Draft'))
                    daTR = daTR == null ? e.Id : daTR;
                else if(e.Correspondence_Type__c.contains('Final'))
                    faTR = faTR == null ? e.Id : faTR;
            }
            
            if(e.Status__c != '')
            {
                IBCForUpdate.put(e.Id, e);
            }
        }

        RecordType rclRT = [select id, name, sObjectType from RecordType where Name = 'Tax and Regional Accounting' and sObjectType ='ENT_Review_Checklist__c' limit 1];
        RecordType AMFART = [select Id, Name, sObjectType from RecordType where sObjectType = 'ENT_Review_Checklist__c' and Name = 'Asset Management' limit 1];
        
        List<ENT_Review_Checklist__c> checklistsForInsert = new List<ENT_Review_Checklist__c>();
        List<ENT_Review_Checklist__c> checklistsForInsert2 = new List<ENT_Review_Checklist__c>();
        List<ENT_Review_Checklist__c> checklistsForInsert3 = new List<ENT_Review_Checklist__c>();
        
        checklistsForInsert.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 1', RecordTypeId = rclRT.Id, Type__c = 'Draft', Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Related_Entity_Type__c = 'Tax Return', Reporting_Checklist__c = testIBC.Id, Reporting_Checklist_Entity_Draft__c = daTR, Reporting_Checklist_Entity_Final__c = faTR));
        checklistsForInsert.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 2', RecordTypeId = rclRT.Id, Type__c = 'Draft', Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Related_Entity_Type__c = 'Financial Statement', Reporting_Checklist__c = testIBC.Id, Reporting_Checklist_Entity_Draft__c = daFS, Reporting_Checklist_Entity_Final__c = faFS));

        checklistsForInsert2.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 3', RecordTypeId = rclRT.Id, Type__c = 'Draft', Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Period__c = 'Quarter 2', Related_Entity_Type__c = 'Financial Statement', Reporting_Checklist__c = testIBC.Id, Reporting_Checklist_Entity_Draft__c = daFS, Reporting_Checklist_Entity_Final__c = faFS));
        checklistsForInsert2.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 4', RecordTypeId = rclRT.Id, Type__c = 'Draft', Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Period__c = 'Quarter 2', Related_Entity_Type__c = 'AMFA', Reporting_Checklist__c = testIBC.Id, Reporting_Checklist_Entity_Draft__c = daFS, Reporting_Checklist_Entity_Final__c = faFS));
        
        checklistsForInsert3.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 3', RecordTypeId = AMFART.Id, Type__c = 'Annual', Uses_Approval_Process__c = true, Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Parent_Checklist__c = null, Period__c = 'Quarter 3', Related_Entity_Type__c = 'AMFA', Reporting_Checklist__c = testIBC.Id, Reporting_Checklist_Entity_Draft__c = daFS, Reporting_Checklist_Entity_Final__c = faFS));
        checklistsForInsert3.add(new ENT_Review_Checklist__c(Name = 'Test Checklist 4', RecordTypeId = AMFART.Id, Type__c = 'Quarterly',Uses_Approval_Process__c = true, Status__c = 'Data Entry Complete', Checklist_Type__c = 'B', Parent_Checklist__c = null, Period__c = 'Quarter 3', Related_Entity_Type__c = 'Tax Return', Reporting_Checklist__c = testIBC.Id, Reporting_Checklist_Entity_Draft__c = daFS, Reporting_Checklist_Entity_Final__c = faFS));
        
        
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        insert checklistsForInsert;
        
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = true;
        Utils.HasRun_ENT_Review_Checklist_Trigger = true;
        insert checklistsForInsert2;
        
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = true;
        Utils.HasRun_ENT_Review_Checklist_Trigger = true;
        insert checklistsForInsert3;
      
        User user1 = [SELECT Id FROM User WHERE Firstname='Brijesh' AND isActive=true limit 1];

        // Create an approval request for the account
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(checklistsForInsert3[0].id);

        // Submit on behalf of a specific submitter
        req1.setSubmitterId(user1.Id); 

        // Submit the record to specific process and skip the criteria evaluation
        req1.setProcessDefinitionNameOrId('RCL_QFS_As_Cs_Clone_2');
        req1.setSkipEntryCriteria(true);

        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);
        
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        update IBCForUpdate.values();

        // v1.2 start/end - re-positioned below method to reset limits.(earlier was on line 464)
        test.startTest();
        // Test Checklist Updates
        List<ENT_Review_Checklist__c> listRCL = [select Id, Name, Type__c, Related_Entity_Type__c, Checklist_Type__c, Status__c
            from ENT_Review_Checklist__c
            where Reporting_Checklist__c = :testIBC.Id];
        
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        for(ENT_Review_Checklist__c rcl : listRCL)
        {
            rcl.Status__c = '';
        }
        update listRCL;
        
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        for(ENT_Review_Checklist__c rcl : listRCL)
        {
            rcl.Status__c = 'In Supervisory Review';
        }
        update listRCL;
        
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        for(ENT_Review_Checklist__c rcl : listRCL)
        {
            rcl.Status__c = 'Approved As Is';
        }
        update listRCL;
        
        // Test AMFA Checklist Linking
        Utils.HasRun_ENT_Inbound_Correspondence_Entity_Create_CL_Trigger = false;
        Utils.HasRun_ENT_Review_Checklist_Trigger = false;
        update checklistsForInsert2;
        
        test.stopTest();
    }
    // v1.1 Ends
}