/***************************************************************
Class   : TEST_TestCoverage_EPIC15
Author  : Bdarji
Date    : 03/15/2020
Details :
          Classes Covered Are:
          -  ENT_CTRL_LIHTC_Late_Notification_R2 (80%)
          -  ENT_Batch_LIHTC_Late_Notify_SetFlag_R2 (100%)
          -  ENT_Batch_LIHTC_Late_Notify_SetFlag_R3 (100%)
History - 03/15/2020 - Bdarji - created
   v1.1 - 04/28/2020 - Bdarji - Added new method to test code of Round-3(EHCI-348)
                              - Also created reminder record for Round-2(in existing method) since we have added logic to identify current reminder.
***************************************************************/
@isTest(SeeAllData = false)
public class TEST_TestCoverage_EPIC15
{
    @testSetup static void setup()
    {
        // get record type for deal
        RecordType dRT = [select id from RecordType where SobjectType = 'ENT_ED_LIHTC__c' and name = 'Standard Deal' limit 1];

        // create project
        ENT_Project__c testP = new ENT_Project__c(Name='Test P');
        INSERT testP;

        // Create Deal
        ENT_ED_LIHTC__c deal = new ENT_ED_LIHTC__c();
        deal.Name = 'testdeal';
        deal.Project_Status__c = 'Closed Pmt Disbursed';
        deal.RecordTypeId = dRT.Id;
        deal.Project__c = testP.Id;
        deal.FundStr__c = 'XYZ';
        INSERT deal;

        List<ENT_ED_LIHTC__c> lstTotalDeals = [Select id, name, FundStr__c from ENT_ED_LIHTC__c];

        List<ENT_Inbound_Correspondence__c> lstTotalRepChklst = [Select id, name, LIHTC_Deal__c,Review_Rating__c, LIHTC_Deal__r.Name from ENT_Inbound_Correspondence__c];
        for(ENT_Inbound_Correspondence__c repChklst : lstTotalRepChklst)
        {
            repChklst.Name = '2019';
        }
        UPDATE lstTotalRepChklst;

        Date DateBefore15FebAnd1March = Date.newInstance(System.Today().year(), 2, 14);

        List<ENT_Inbound_Correspondence_Entity__c> lstTotalEntities = [Select id, name,Correspondence_Type__c,Reporting_Year__c,Date_Received__c ,Anticipated_Received_Date__c ,Inbound_Correspondence__r.Review_Rating__c, Inbound_Correspondence__c, Inbound_Correspondence__r.LIHTC_Deal__r.Name from ENT_Inbound_Correspondence_Entity__c];
        for(ENT_Inbound_Correspondence_Entity__c entity: lstTotalEntities)
        {
            entity.Date_Received__c = null;
            entity.Anticipated_Received_Date__c = system.today().addDays(-7);
            if(entity.Correspondence_Type__c == 'Draft Tax Return' || entity.Correspondence_Type__c == 'Draft Financial Statement')
            {
                entity.Deadline_Date__c = DateBefore15FebAnd1March;
            }
            if(entity.Correspondence_Type__c == 'Final Tax Return' || entity.Correspondence_Type__c == 'Final Financial Statement')
            {
                entity.Deadline_Date__c = DateBefore15FebAnd1March;
            }
        }
        UPDATE lstTotalEntities;

        RecordType acctRT = [SELECT Id, Name, SobjectType, IsActive FROM RecordType where SobjectType = 'Account' and Name = 'Standard'];

        // create account
        Account testAcctCohnReznick = new Account(Name = ' CohnReznick LLP (New York)', RecordTypeId = acctRT.Id);
        INSERT testAcctCohnReznick;

        // create contact
        Contact testContactCohnReznick = new Contact(FirstName = 'Cohn', LastName = 'Reznick', AccountId = testAcctCohnReznick.Id);
        INSERT testContactCohnReznick;

        // create business relationship for FRC external resource.
        ENT_Business_Relationship__c testRelationshipCohnReznick = [select id, name, account__c, contact__c, contact__r.firstname, contact__r.lastname from ENT_Business_Relationship__c where Contact__c = :testContactCohnReznick.Id limit 1];

        RecordType teamOppRT = [SELECT Id, Name, SobjectType, IsActive FROM RecordType where SobjectType = 'ENT_External_Resource__c' and Name = 'LIHTC Deal External Resource'];

        // create active and primary FRC external resource
        ENT_External_Resource__c er1 = new ENT_External_Resource__c();
        er1.Account__c = testAcctCohnReznick.Id;
        er1.Person__c = testRelationshipCohnReznick.Id;
        er1.primary__c=true;
        er1.Contact_Role__c = 'Financial Reporting Contact';
        er1.RecordTypeId = teamOppRT.Id;
        er1.LIHTC_Deal__c = lstTotalRepChklst[0].LIHTC_Deal__c;
        er1.Role__c = 'CPA Firm';
        er1.Active__c = true;
        er1.Start_Date__c = system.today().addDays(-7);
        INSERT er1;
    }

    static testMethod void Test_ENT_Batch_LIHTC_Late_Notify_SetFlag_R2_w_MissingDocs()
    {
        // v1.1 Starts
        // Create Reminder for Round-2, since we have added logic to identify reminder and update field accordingly.
        Date DateStart = Date.newInstance(2020, 1, 1);
        Date DateEnd = Date.newInstance(2050, 1, 1);
        Date DateDueR2 = Date.newInstance(2020, 4, 21);

        ENT_Community_Reminder__c crR2 = new ENT_Community_Reminder__c();
        crR2.Title__c = 'TestCase';
        crR2.Related_Community_Application__c = 'Custom';
        crR2.Internal_Name__c = '2019 - 12/31 Round 2 (Draft/Final Required) Late Notice - FRC';
        crR2.Notification_Start_Date__c = DateStart;
        crR2.Notification_Type__c = 'One Time';
        crR2.Notification_End_Date__c = DateEnd;
        crR2.Active__c = true;
        crR2.Due_Date__c = DateDueR2;
        crR2.Content_Type__c = 'Email Template';
        crR2.Email_Template__c = '2019 - 12/31 Round 2 (Draft/Final Required) Late Notice - FRC';
        crR2.Sent_From__c = '   financialreporting@enterprisecommunity.com';
        crR2.Type__c = 'SOQL';
        crR2.SOQL__c = 'SELECT Person__r.Contact__c FROM ENT_External_Resource__c WHERE Contact_Role__c = \'Financial Reporting Contact\' AND primary__c = true AND Active__c = true AND ((LIHTC_Deal__r.Project_Status__c = \'Closed Pmt Disbursed\' AND Actual_Closing_Date__c != THIS_YEAR) OR (LIHTC_Deal__r.Project_Status__c = \'Sold\' AND (LIHTC_Deal__r.Sold_Date_Actual__c > LAST_YEAR OR LIHTC_Deal__r.Sold_Date_Actual__c = null))) AND (LIHTC_Deal__r.FundStr__c != null) AND (NOT LIHTC_Deal__r.FundStr__c like \'%NYEF%\') AND (NOT LIHTC_Deal__r.FundStr__c like \'%EMOF%\') AND (NOT LIHTC_Deal__r.FundStr__c like \'%481%\') AND (NOT LIHTC_Deal__r.FundStr__c like \'%TDBUSA%\') AND (LIHTC_Deal__r.Asset_Manager__r.Name != \'Monica Spillane\') and LIHTC_Deal__r.X12_31_Round_2_Missing_Documents__c != \'\' limit 10';
        Insert crR2;
        // v1.1 Ends

        Test.startTest();
        ENT_Batch_LIHTC_Late_Notify_SetFlag_R2 btch = new ENT_Batch_LIHTC_Late_Notify_SetFlag_R2();
        Database.executeBatch(btch);
        Test.stopTest();

        List<ENT_ED_LIHTC__c> lstDealAfterBatch = [select id, name, X12_31_Round_2_Missing_Documents__c from ENT_ED_LIHTC__c];
        system.assertEquals('Draft Financial Statement#Draft Tax Return', lstDealAfterBatch[0].X12_31_Round_2_Missing_Documents__c);
    }

    // v1.1 Starts
    // Added method to test logic of 'ENT_Batch_LIHTC_Late_Notify_SetFlag_R2' for Round-3
    static testMethod void Test_ENT_Batch_LIHTC_Late_Notify_SetFlag_R2_w_MissingDocsR3()
    {
        // Create Reminder for Round-3, since we have added logic to identify reminder and update field accordingly.
        Date DateStart = Date.newInstance(2020, 1, 1);
        Date DateEnd = Date.newInstance(2050, 1, 1);
        Date DateDueR3 = Date.newInstance(2020, 4, 30);

        ENT_Community_Reminder__c crR3 = new ENT_Community_Reminder__c();
        crR3.Title__c = 'TestCase';
        crR3.Related_Community_Application__c = 'Custom';
        crR3.Internal_Name__c = '2019 - 12/31 Round 3 (Draft/Final Required) Late Notice - Sponsor';
        crR3.Notification_Start_Date__c = DateStart;
        crR3.Notification_Type__c = 'One Time';
        crR3.Notification_End_Date__c = DateEnd;
        crR3.Active__c = true;
        crR3.Due_Date__c = DateDueR3;
        crR3.Content_Type__c = 'Email Template';
        crR3.Email_Template__c = '2019 - 12/31 Round 3 (Draft/Final Required) Late Notice - Sponsor';
        crR3.Sent_From__c = '   financialreporting@enterprisecommunity.com';
        crR3.Type__c = 'SOQL';
        crR3.SOQL__c = 'SELECT Person__r.Contact__c FROM ENT_External_Resource__c WHERE Contact_Role__c = \'Sponsor Contact\' AND primary__c = true AND Active__c = true AND ((LIHTC_Deal__r.Project_Status__c = \'Closed Pmt Disbursed\' AND Actual_Closing_Date__c != THIS_YEAR) OR (LIHTC_Deal__r.Project_Status__c = \'Sold\' AND (LIHTC_Deal__r.Sold_Date_Actual__c > LAST_YEAR OR LIHTC_Deal__r.Sold_Date_Actual__c = null))) AND (LIHTC_Deal__r.FundStr__c != null) AND (NOT LIHTC_Deal__r.FundStr__c like \'%NYEF%\') AND (NOT LIHTC_Deal__r.FundStr__c like \'%EMOF%\') AND (NOT LIHTC_Deal__r.FundStr__c like \'%481%\') AND (NOT LIHTC_Deal__r.FundStr__c like \'%TDBUSA%\') AND (LIHTC_Deal__r.Asset_Manager__r.Name != \'Monica Spillane\') and LIHTC_Deal__r.X12_31_Round_3_Missing_Documents__c != \'\'';
        Insert crR3;

        Test.startTest();
        ENT_Batch_LIHTC_Late_Notify_SetFlag_R2 btch = new ENT_Batch_LIHTC_Late_Notify_SetFlag_R2();
        Database.executeBatch(btch);
        Test.stopTest();

        List<ENT_ED_LIHTC__c> lstDealAfterBatch = [select id, name, X12_31_Round_2_Missing_Documents__c from ENT_ED_LIHTC__c];
        system.assertEquals(null, lstDealAfterBatch[0].X12_31_Round_2_Missing_Documents__c);
    }
    // v1.1 Ends

    static testMethod void Test_ENT_Batch_LIHTC_Late_Notify_SetFlag_R2_wo_MissingDocs()
    {
        // get record type for deal
        RecordType dRT = [select id from RecordType where SobjectType = 'ENT_ED_LIHTC__c' and name = 'Standard Deal' limit 1];

        // create project
        ENT_Project__c testP = new ENT_Project__c(Name='Test P');
        INSERT testP;

        // Create Deal
        ENT_ED_LIHTC__c deal = new ENT_ED_LIHTC__c();
        deal.Name = 'testdeal';
        deal.Project_Status__c = 'Closed Pmt Disbursed';
        deal.RecordTypeId = dRT.Id;
        deal.Project__c = testP.Id;
        deal.FundStr__c = 'XYZ';
        deal.X12_31_Round_2_Missing_Documents__c = 'abc';
        INSERT deal;

        Test.startTest();
        ENT_Batch_LIHTC_Late_Notify_SetFlag_R2 btch = new ENT_Batch_LIHTC_Late_Notify_SetFlag_R2();
        Database.executeBatch(btch);
        Test.stopTest();

        List<ENT_ED_LIHTC__c> lstDealAfterBatch1 = [select id, name, X12_31_Round_2_Missing_Documents__c from ENT_ED_LIHTC__c where id=: deal.Id];
        system.assertEquals('abc', lstDealAfterBatch1[0].X12_31_Round_2_Missing_Documents__c);
    }

    // Below method will test logic/functionality written in 'ENT_Batch_LIHTC_Late_Notify_SetFlag_R2' class.
    public static Testmethod void Test_ENT_Batch_LIHTC_Late_Notify_SetFlag_R2_Scheduler()
    {
        // Define cron expression same as used in batch.
        String CRON_EXP = '0 0 0 1 JAN,APR,JUL,OCT ?';

        Test.startTest();
        Id jobId = System.schedule('TestSetFlag ENT_Batch_LIHTC_Late_Notify_SetFlag_R2', CRON_EXP, new ENT_Batch_LIHTC_Late_Notify_SetFlag_R2());
        Test.stopTest();

        // Verify job is scheduled with same cron expression.
        List<CronTrigger> ct = [SELECT Id, CronJobDetailId, NextFireTime, PreviousFireTime, StartTime, CronExpression, CreatedDate, TimesTriggered FROM CronTrigger where CronExpression = '0 0 0 1 JAN,APR,JUL,OCT ?' order by CreatedDate desc limit 1];
        System.assertEquals(ct[0].Id, jobId);
    }

    static testMethod void Test_ENT_CTRL_LIHTC_Late_Notification_R2_w_MissingDocs()
    {
        // get contact list created from test method.
        List<Contact> lstContact = [Select Id, Name, FirstName, LastName, AccountId  From Contact];

        List<ENT_ED_LIHTC__c> lstTotalDeals = [Select id, name,X12_31_Round_2_Missing_Documents__c , FundStr__c from ENT_ED_LIHTC__c];
        for(ENT_ED_LIHTC__c deal: lstTotalDeals)
        {
            deal.X12_31_Round_2_Missing_Documents__c = 'Draft Tax Return#Final Tax Return';
        }
        UPDATE lstTotalDeals;

        // get a user
        List<User> lstUsers = [SELECT Id, Name FROM User WHERE Profile.Name like 'LIHTC%' LIMIT 1];

        if(!lstUsers.isEmpty())
        {
            // create enterprise team member with accounting manager role
            ENT_Enterprise_Team__c et = new ENT_Enterprise_Team__c();
            et.User__c = lstUsers[0].Id;
            et.Role__c = 'Accounting Manager';
            et.Primary__c = true;
            et.Active__c = true;
            et.LIHTC_Deal__c = lstTotalDeals[0].Id;
            INSERT et;
        }

        // call a class and initialize personId
        Test.startTest();
        ENT_CTRL_LIHTC_Late_Notification_R2 ctrl = new ENT_CTRL_LIHTC_Late_Notification_R2();
        ctrl.PersonId = lstContact[0].Id;
        Test.stopTest();

        // Get a string which accepts 'RecipientId' variable in vf component.
        Id aString = ctrl.RecipientId;

        // check map is not null
        system.assertNotEquals(null, ctrl.mapOfDealWithMissingEntitiesRound2);
    }

    // v1.1 Starts
    // Added method to test logic of 'ENT_CTRL_LIHTC_Late_Notification_R3'.
    static testMethod void Test_ENT_CTRL_LIHTC_Late_Notification_R3_w_MissingDocs()
    {
        // Get deals created and update assign field value as required for this class.
        List<ENT_ED_LIHTC__c> lstTotalDeals = [Select id, name,X12_31_Round_3_Missing_Documents__c , FundStr__c from ENT_ED_LIHTC__c];

        Date DateOfClosingDeal = Date.newInstance(2017, 1, 1);

        for(ENT_ED_LIHTC__c deal: lstTotalDeals)
        {
            deal.X12_31_Round_3_Missing_Documents__c = 'Draft Tax Return#Final Tax Return';
            deal.Actual_Closing_Date__c = DateOfClosingDeal;
        }
        UPDATE lstTotalDeals;

        // Get recordtype of account.
        RecordType acctRT = [SELECT Id, Name, SobjectType, IsActive FROM RecordType where SobjectType = 'Account' and Name = 'Standard'];

        // create account
        Account testAcct = new Account(Name = 'test', RecordTypeId = acctRT.Id);
        INSERT testAcct;

        // create contact
        Contact testContact = new Contact(FirstName = 'test', LastName = 'account', AccountId = testAcct.Id);
        INSERT testContact;

        // create business relationship for FRC external resource.
        ENT_Business_Relationship__c testRelationship = [select id, name, account__c, contact__c, contact__r.firstname, contact__r.lastname from ENT_Business_Relationship__c where Contact__c = :testContact.Id limit 1];

        // Get record type for external resource.
        RecordType teamOppRT = [SELECT Id, Name, SobjectType, IsActive FROM RecordType where SobjectType = 'ENT_External_Resource__c' and Name = 'LIHTC Deal External Resource'];

        // TEST.STARTTEST ADDED HERE TO RESET SOQL LIMIT, AND AVOID 'TOO MANY QSOQL' ERROR.
        Test.startTest();

        // create active and primary 'Sponsor Contact'
        ENT_External_Resource__c sponsor = new ENT_External_Resource__c();
        sponsor.Account__c = testAcct.Id;
        sponsor.Person__c = testRelationship.Id;
        sponsor.primary__c=true;
        sponsor.Contact_Role__c = 'Sponsor Contact';
        sponsor.RecordTypeId = teamOppRT.Id;
        sponsor.LIHTC_Deal__c = lstTotalDeals[0].Id;
        sponsor.Active__c = true;
        sponsor.Start_Date__c = system.today().addDays(-7);
        INSERT sponsor;

        // get a user
        List<User> lstUsers = [SELECT Id, Name FROM User WHERE Profile.Name like 'LIHTC%' LIMIT 1];
        if(!lstUsers.isEmpty())
        {
            // create enterprise team member with accounting manager role
            ENT_Enterprise_Team__c et = new ENT_Enterprise_Team__c();
            et.User__c = lstUsers[0].Id;
            et.Role__c = 'Accounting Manager';
            et.Primary__c = true;
            et.Active__c = true;
            et.LIHTC_Deal__c = lstTotalDeals[0].Id;
            INSERT et;
        }

        // call a class and initialize personId
        ENT_CTRL_LIHTC_Late_Notification_R3 ctrl = new ENT_CTRL_LIHTC_Late_Notification_R3();
        ctrl.PersonId = testContact.Id;

        // Get a string which accepts 'RecipientId' variable in vf component.
        Id aString = ctrl.RecipientId;
        Test.stopTest();

        // check map is not null
        system.assertNotEquals(null, ctrl.mapOfDealWithMissingEntitiesRound3);
    }
    // v1.1 Ends
}